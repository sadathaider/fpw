package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.codercrew.fpw.Utils.Config;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;


public class SplashScreen extends AppCompatActivity {
    SharedPrefrencesMain sharedPrefrencesMain;
    private static int SPLASH_TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_splash);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        splash();
    }

    public void splash() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                if (sharedPrefrencesMain.getisLogin()) {

                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    if(getIntent().getExtras()!=null && getIntent().getExtras().get("User")!=null) {
//                        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                        String json = getIntent().getExtras().get("User").toString();
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            i.putExtra("message", "Its "+jsonObject.getString("name")+" Birthday");
                            i.putExtra("title", "Birthday Alert");
                            i.putExtra("body", jsonObject.getString("id"));
//                            LocalBroadcastManager.getInstance(SplashScreen.this).sendBroadcast(pushNotification);

                        } catch (JSONException jsonException) {
                            jsonException.printStackTrace();
                        }
                    }
                    startActivity(i);
                    finish();
                } else {
                    // checkFirstRun();
                    if(sharedPrefrencesMain.getLoginStep().isEmpty()) {
                        Intent i = new Intent(SplashScreen.this, LoginAndSigUpActivity.class);
                        startActivity(i);
                        finish();
                    }else if(sharedPrefrencesMain.getLoginStep().equals("one")){
                        Intent intent = new Intent(SplashScreen.this,EmailVerificationActivity.class);
                        intent.putExtra("email",sharedPrefrencesMain.getEmail());
                        startActivity(intent);
                    }
                }

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}