package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyModel;

import java.util.ArrayList;
import java.util.List;

public class SwitchFamilyAdapter extends RecyclerView.Adapter<SwitchFamilyAdapter.BlockedFamilyMembersViewHolder> {

    List<FamilyModel> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public SwitchFamilyAdapter(Context context, List<FamilyModel> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public BlockedFamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.switch_families_row_layout,parent,false);
        return new BlockedFamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedFamilyMembersViewHolder holder, int position) {

        holder.textViewName.setText(familyMembersModelList.get(position).getFamily());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class BlockedFamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName;


        public BlockedFamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.tvName);
        }
    }
}
