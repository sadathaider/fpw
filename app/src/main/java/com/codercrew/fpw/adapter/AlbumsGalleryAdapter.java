package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class AlbumsGalleryAdapter extends RecyclerView.Adapter<AlbumsGalleryAdapter.LikesViewHolder> {

    ArrayList<GetAlbumPhotos.Data> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public AlbumsGalleryAdapter(Context context, ArrayList<GetAlbumPhotos.Data> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public LikesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.albums_gallery_row_layout,parent,false);
        return new LikesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LikesViewHolder holder, int position) {
        holder.imageViewDel.setVisibility(View.VISIBLE);
        if(familyMembersModelList.get(position).getImage()!=null) {
            if (!familyMembersModelList.get(position).getImage().isEmpty())
                Glide.with(context).load(familyMembersModelList.get(position).getImage()).into(holder.imageViewProfile);
        }
        if(familyMembersModelList.get(position).getVideo()!=null) {
            if (!familyMembersModelList.get(position).getVideo().isEmpty()) {
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions.isMemoryCacheable();
                Glide.with(context).load(familyMembersModelList.get(position).getThumb()).into(holder.imageViewProfile);
            }
        }


        holder.imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
        holder.imageViewDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    public void clearItem(int pos) {
        familyMembersModelList.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class LikesViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewDel;
        private TextView textViewName;

        public LikesViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            imageViewDel = itemView.findViewById(R.id.imgDelete);
//            textViewName = itemView.findViewById(R.id.tvName);

        }
    }
}
