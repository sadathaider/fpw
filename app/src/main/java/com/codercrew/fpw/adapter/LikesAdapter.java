package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetLikes;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.LikesViewHolder> {

    ArrayList<GetLikes.LikesDetails> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public LikesAdapter(Context context, ArrayList<GetLikes.LikesDetails> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public LikesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.likes_row_layout,parent,false);
        return new LikesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LikesViewHolder holder, int position) {

        if(!familyMembersModelList.get(position).getLikerprofilepic().isEmpty())
            Glide.with(context).load(familyMembersModelList.get(position).getLikerprofilepic()).into(holder.imageViewProfile);
        holder.textViewName.setText(familyMembersModelList.get(position).getLikername());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class LikesViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName;


        public LikesViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewName = itemView.findViewById(R.id.tvName);

        }
    }
}
