package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetComments;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    ArrayList<GetComments.CommentsDetails> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;
    SharedPrefrencesMain sharedPrefrencesMain;

    public CommentsAdapter(Context context, ArrayList<GetComments.CommentsDetails> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
        sharedPrefrencesMain = new SharedPrefrencesMain(context);
    }


    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.comments_row_layout,parent,false);
        return new CommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {

        holder.imageViewMenu.setVisibility(View.GONE);
        holder.textViewName.setText(familyMembersModelList.get(position).getCommentername());
        holder.textViewComment.setText(familyMembersModelList.get(position).getComment());
        if(!familyMembersModelList.get(position).getCommenterprofilepic().isEmpty())
            Glide.with(context).load(familyMembersModelList.get(position).getCommenterprofilepic()).into(holder.imageViewProfile);

        if(familyMembersModelList.get(position).getCommenterid().equals(sharedPrefrencesMain.getId())){
            holder.imageViewMenu.setVisibility(View.VISIBLE);
        }
        holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName,textViewComment;


        public CommentsViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewName = itemView.findViewById(R.id.tvName);
            textViewComment = itemView.findViewById(R.id.tvComments);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
        }
    }
}
