package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.model.GetUsers;
import com.codercrew.fpw.model.User;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class BlockedFamilyMemberAdapter extends RecyclerView.Adapter<BlockedFamilyMemberAdapter.BlockedFamilyMembersViewHolder> {

    List<User> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public BlockedFamilyMemberAdapter(Context context, List<User> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public BlockedFamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.blocked_family_members_row_layout,parent,false);
        return new BlockedFamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedFamilyMembersViewHolder holder, int position) {

        if(familyMembersModelList.get(position).getProfilepic()!=null && familyMembersModelList.get(position).getProfilepic().length()>0)
            Glide.with(context).load(familyMembersModelList.get(position).getProfilepic()).into(holder.imageViewProfile);
        else Glide.with(context).load(R.drawable.user_profile).into(holder.imageViewProfile);

        holder.textViewName.setText(familyMembersModelList.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class BlockedFamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName;


        public BlockedFamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewName = itemView.findViewById(R.id.tvName);
        }
    }
}
