package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyMembersModel;

import java.util.ArrayList;
import java.util.List;

public class FamilyMemberWallsAdapter extends RecyclerView.Adapter<FamilyMemberWallsAdapter.FamilyMembersViewHolder> {

    List<FamilyMembersModel> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public FamilyMemberWallsAdapter(Context context, List<FamilyMembersModel> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public FamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.family_members_wall_row_layout,parent,false);
        return new FamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyMembersViewHolder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class FamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;


        public FamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
