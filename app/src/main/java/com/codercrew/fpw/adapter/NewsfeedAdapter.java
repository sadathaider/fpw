package com.codercrew.fpw.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetPost;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.codercrew.fpw.model.GetPostPagination;

import java.util.ArrayList;
import java.util.HashMap;

public class NewsfeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<GetPostPagination.Data.PostDetails> postDetails = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;
    SharedPrefrencesMain sharedPrefrencesMain;
    public static String comment;

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    public NewsfeedAdapter(Context context, ArrayList<GetPostPagination.Data.PostDetails> postDetails, OnItemClick onItemClick){
        this.context = context;
        this.postDetails = postDetails;
        this.onItemClick = onItemClick;
        sharedPrefrencesMain = new SharedPrefrencesMain(context);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new NewsfeedViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.newsfeed_row_layout, parent, false));
            case VIEW_TYPE_LOADING:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }
//        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//        View view = layoutInflater.inflate(R.layout.newsfeed_row_layout,parent,false);
//        return new NewsfeedViewHolder(view);
    }

//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//
//    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vholder, int position) {
        if(getItemViewType(position)==VIEW_TYPE_NORMAL) {
            NewsfeedViewHolder holder = (NewsfeedViewHolder) vholder;
            holder.imageViewWall.setVisibility(View.GONE);
            holder.textViewDesc.setVisibility(View.GONE);
            holder.imageViewPlay.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.GONE);
            if (postDetails.get(position).getPosterid()!=null && sharedPrefrencesMain.getId().equals(postDetails.get(position).getPosterid()))
                holder.imageViewMenu.setVisibility(View.VISIBLE);
            else holder.imageViewMenu.setVisibility(View.GONE);

            holder.textViewName.setText(postDetails.get(position).getPostername());
            holder.textViewDuration.setText(utils.getTimeAgo(Long.parseLong(postDetails.get(position).getCreatedAt())));
            holder.textViewLikes.setText(postDetails.get(position).getLikecount() + "Likes");
            if (postDetails.get(position).getCommentcount().equals("0"))
                holder.textViewComments.setText(postDetails.get(position).getCommentcount() + " comments");
            else
                holder.textViewComments.setText("View all " + postDetails.get(position).getCommentcount() + " comments");

            if (postDetails.get(position).getIsliked() != null && postDetails.get(position).getIsliked().equals("1"))
                holder.imageViewLike.setImageResource(R.drawable.like_icon);
            else holder.imageViewLike.setImageResource(R.drawable.like_icon_empty);

            if (postDetails.get(position).getPosterimage() != null && !postDetails.get(position).getPosterimage().isEmpty())
                Glide.with(context).load(postDetails.get(position).getPosterimage()).into(holder.imageViewProfile);
            else holder.imageViewProfile.setImageResource(R.drawable.user_profile);

            if (!postDetails.get(position).getText().isEmpty()) {
                holder.textViewDesc.setVisibility(View.VISIBLE);
                holder.textViewDesc.setText(postDetails.get(position).getText());
            }

            if (postDetails.get(position).getType().equals("1")) {
                holder.progressBar.setVisibility(View.GONE);
                holder.imageViewWall.setVisibility(View.VISIBLE);
                if (postDetails.get(position).getImage() != null && postDetails.get(position).getImage().length() > 0)
                    Glide.with(context).load(postDetails.get(position).getImage()).into(holder.imageViewWall);

//                .addListener(new RequestListener<Drawable>() {
//                        @Override
//                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                            holder.progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//                    }).into(holder.imageViewWall);

            } else if (postDetails.get(position).getType().equals("0")) {
                holder.textViewDesc.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.GONE);
                holder.textViewDesc.setText(postDetails.get(position).getText());
            } else if (postDetails.get(position).getType().equals("2")) {
                holder.progressBar.setVisibility(View.GONE);
                holder.imageViewWall.setVisibility(View.VISIBLE);
                if (postDetails.get(position).getImage() != null && postDetails.get(position).getVideo().length() > 0) {
                    holder.imageViewPlay.setVisibility(View.VISIBLE);
//                    RequestOptions requestOptions = new RequestOptions();
//                    requestOptions.isMemoryCacheable();
                    Glide.with(context).load(postDetails.get(position).getThumb()).into(holder.imageViewWall);
//                    Glide.with(context).load(postDetails.get(position).getThumb()).addListener(new RequestListener<Drawable>() {
//                        @Override
//                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                            holder.progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//                    }).into(holder.imageViewWall);
                }
            }
            holder.editTextComment.setError(null);

            holder.imageViewSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.editTextComment.getText().toString().isEmpty()) {
                        holder.editTextComment.setError("Enter comment");
                        holder.editTextComment.requestFocus();
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        return;
                    }
                    comment = holder.editTextComment.getText().toString();
                    holder.editTextComment.setText("");
                    holder.editTextComment.clearFocus();
                    onItemClick.onClick(v, position);
                }
            });

            holder.imageViewCmt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    holder.editTextComment.setError("Enter comment");
                    holder.editTextComment.requestFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            });

            holder.imageViewLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });
            holder.textViewComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });
            holder.textViewLikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });

            holder.imageViewWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });

            holder.imageViewPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });

            holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onClick(v, position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == postDetails.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    public void addItems(ArrayList<GetPostPagination.Data.PostDetails> postItems) {
        postDetails.addAll(postItems);
        notifyDataSetChanged();
    }
    public void addLoading() {
        isLoaderVisible = true;
        postDetails.add(null);
        notifyItemInserted(postDetails.size() - 1);
    }

    public void clearItems() {
        postDetails.clear();
        notifyDataSetChanged();
    }
    public void clearItem(int pos) {
        postDetails.remove(pos);
        notifyDataSetChanged();
    }
    public void removeLoading() {
        isLoaderVisible = false;
        int position = postDetails.size() - 1;
        GetPostPagination.Data.PostDetails item = getItem(position);
        if (item == null) {
            postDetails.remove(position);
            notifyItemRemoved(position);
        }
    }

    GetPostPagination.Data.PostDetails getItem(int position) {
        return postDetails.get(position);
    }

    @Override
    public int getItemCount() {
        return postDetails == null ? 0 : postDetails.size();
//        return postDetails.size();
    }

    public class NewsfeedViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewWall, imageViewProfile,imageViewMenu,imageViewLike,imageViewPlay,imageViewCmt;
        private TextView textViewDuration,textViewLikes,textViewDesc,textViewComments,textViewName;
        private EditText editTextComment;
        private ImageView imageViewSend;
        ProgressBar progressBar;

        public NewsfeedViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.tvName);
            textViewDuration = itemView.findViewById(R.id.tvDuration);
            imageViewWall = itemView.findViewById(R.id.imgWall);
            textViewLikes = itemView.findViewById(R.id.tvLikes);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
            textViewDesc = itemView.findViewById(R.id.tvDesc);
            imageViewLike = itemView.findViewById(R.id.imgLike);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewComments = itemView.findViewById(R.id.tvComments);
            imageViewPlay = itemView.findViewById(R.id.imgPlay);
            editTextComment = itemView.findViewById(R.id.tvAddCmt);
            imageViewSend = itemView.findViewById(R.id.imgSend);
            imageViewCmt = itemView.findViewById(R.id.imgComment);
            progressBar = itemView.findViewById(R.id.progress);

        }
    }

    public class ProgressHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;
        ProgressHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
