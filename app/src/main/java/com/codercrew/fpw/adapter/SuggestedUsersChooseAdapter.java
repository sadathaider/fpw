package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.AnimatorRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.User;

import java.util.ArrayList;
import java.util.List;

public class SuggestedUsersChooseAdapter extends RecyclerView.Adapter<SuggestedUsersChooseAdapter.FamilyMembersViewHolder> implements Filterable {

    List<User> familyMembersModelList = new ArrayList<>();
    List<User> familyMembersModelListAll = new ArrayList<>();
    public ArrayList<User> selectedUsers = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public SuggestedUsersChooseAdapter(Context context, List<User> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        familyMembersModelListAll = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public FamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.suggested_members_row_layout,parent,false);
        return new FamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyMembersViewHolder holder, int position) {
        holder.imageViewProfile.setImageResource(R.drawable.user_profile);
        holder.textViewName.setText(familyMembersModelList.get(position).getName());
        if(familyMembersModelList.get(position).getProfilepic().length()>0)
            Glide.with(AppContext.getAppContext()).load(familyMembersModelList.get(position).getProfilepic()).into(holder.imageViewProfile);

        if(!familyMembersModelList.get(position).isSelected())
            holder.imageViewMenu.setImageResource(R.drawable.un_select);
        else {
            holder.imageViewMenu.setImageResource(R.drawable.selected);
            if(!selectedUsers.contains(familyMembersModelList.get(position)))
                selectedUsers.add(familyMembersModelList.get(position));
        }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });

        holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    public User GetSelectedUser(int pos){
        return familyMembersModelList.get(pos);
    }

    public ArrayList<User> GetSelectedUsers(){
        return selectedUsers;
    }
    public void SelectUser(int pos){
        if(!familyMembersModelList.get(pos).isSelected()) {
            familyMembersModelList.get(pos).setSelected(true);
            selectedUsers.add(familyMembersModelList.get(pos));
        }
        else {
            familyMembersModelList.get(pos).setSelected(false);
            selectedUsers.remove(familyMembersModelList.get(pos));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class FamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName;


        public FamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
            textViewName = itemView.findViewById(R.id.tvName);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                familyMembersModelList = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<User> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = familyMembersModelListAll;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }
    protected ArrayList<User> getFilteredResults(String constraint) {
        ArrayList<User> results = new ArrayList<>();
        for (User item : familyMembersModelListAll) {
            if (item.getName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }
}
