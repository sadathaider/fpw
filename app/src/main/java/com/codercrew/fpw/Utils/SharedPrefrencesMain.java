package com.codercrew.fpw.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.codercrew.fpw.model.User;

public class SharedPrefrencesMain {
    Context context;
    private static SharedPrefrencesMain sharedPrefrencesMain;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    public SharedPrefrencesMain(Context context){
        this.context=context;
        sharedPreferences=context.getSharedPreferences("preference",Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();

    }
    public static SharedPrefrencesMain getInstance(Context context){
         if (sharedPrefrencesMain==null){
        sharedPrefrencesMain=new SharedPrefrencesMain(context);
        return sharedPrefrencesMain;

    }
        return sharedPrefrencesMain;

}
    public void removePreference(){
        editor=sharedPreferences.edit();
        editor.clear().apply();
    }

    public  void setIsLogin(Boolean isLogin){
        editor=sharedPreferences.edit();
        editor.putBoolean("isLogin",isLogin);
        editor.apply();
    }
    public Boolean getisLogin(){

        return sharedPreferences.getBoolean("isLogin",false);
    }

    public  void setToken(String token){
        editor=sharedPreferences.edit();
        editor.putString("token",token);
        editor.apply();
    }

    public  void setLoginStep(String step){
        editor=sharedPreferences.edit();
        editor.putString("step",step);
        editor.apply();
    }

    public String getLoginStep(){
        return sharedPreferences.getString("step","");
    }

    public  void setFcount(String step){
        editor=sharedPreferences.edit();
        editor.putString("fcount",step);
        editor.apply();
    }
    public String getFcount(){
        return sharedPreferences.getString("fcount","");
    }

    public  void setPlan(String step){
        editor=sharedPreferences.edit();
        editor.putString("plan",step);
        editor.apply();
    }
    public String getPlan(){
        return sharedPreferences.getString("plan","");
    }


    public  void setUser(User user){
        editor=sharedPreferences.edit();
        editor.putString("id",user.getId());
        editor.putString("name",user.getName());
        editor.putString("email",user.getEmail());
        editor.putString("family",user.getFamily());
        editor.putString("familyCode",user.getFamilyCode());
        editor.putString("profileUrl",user.getProfilepic());
        editor.putString("dob",user.getDob());
        editor.putString("country",user.getCountry());
        editor.putString("city",user.getCity());
        editor.putString("posCode",user.getZip());
        editor.putString("state",user.getState());
        editor.putString("gender",user.getGender()+"");
        editor.apply();
    }

    public void setCountryId(String id){
        editor=sharedPreferences.edit();
        editor.putString("countryId",id);
        editor.apply();
    }
    public String getCountryId(){
        return sharedPreferences.getString("countryId","");
    }

    public void setStateId(String id){
        editor=sharedPreferences.edit();
        editor.putString("stateId",id);
        editor.apply();
    }
    public String getStateId(){
        return sharedPreferences.getString("stateId","");
    }

    public void setCountryFlag(int flag){
        editor=sharedPreferences.edit();
        editor.putInt("countryFlag",flag);
        editor.apply();
    }

    public int getCountryFlag(){
        return sharedPreferences.getInt("countryFlag",0);
    }

    public void setEmail(String email){
        editor=sharedPreferences.edit();
        editor.putString("email",email);
        editor.apply();
    }

    public String getId(){
        return sharedPreferences.getString("id","");
    }
    public String getName(){
        return sharedPreferences.getString("name","");
    }
    public String getEmail(){
        return sharedPreferences.getString("email","");
    }
    public String getFamily(){
        return sharedPreferences.getString("family","");
    }
    public String getFmCode(){
        return sharedPreferences.getString("familyCode","");
    }
    public String getProfileUrl(){
        return sharedPreferences.getString("profileUrl","");
    }
    public String getDob(){
        return sharedPreferences.getString("dob","");
    }
    public String getCountry(){
        return sharedPreferences.getString("country","");
    }
    public String getCity(){
        return sharedPreferences.getString("city","");
    }
    public String getPosCode(){
        return sharedPreferences.getString("posCode","");
    }
    public String getState(){
        return sharedPreferences.getString("state","");
    }
    public String getGender(){
        return sharedPreferences.getString("gender","");
    }


    public String getToken(){

        return sharedPreferences.getString("token","");
    }

    public  boolean Logout(){
        editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

}
