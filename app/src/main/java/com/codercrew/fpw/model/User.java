
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

//    @SerializedName("createdAt")
//    @Expose
//    private Integer createdAt;
//
//    @SerializedName("updatedAt")
//    @Expose
//    private Integer updatedAt;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("familyCode")
    @Expose
    private String familyCode;
    @SerializedName("profilepic")
    @Expose
    private String profilepic;
    @SerializedName("fcmToken")
    @Expose
    private String fcmToken;
    @SerializedName("san")
    @Expose
    private String san;
    @SerializedName("verify")
    @Expose
    private Integer verify;

    @SerializedName("relation")
    @Expose
    private String relation;

    @SerializedName("wall")
    @Expose
    private Wall wall;

    @SerializedName("selected")
    @Expose
    private boolean selected;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("blocked")
    @Expose
    private String blocked;

    @SerializedName("deceased")
    @Expose
    private String deceased;

    @SerializedName("reportCount")
    @Expose
    private String reportCount;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("canmessage")
    @Expose
    private boolean canmessage;

    @SerializedName("plan")
    @Expose
    private String plan;

    @SerializedName("fcount")
    @Expose
    private String fcount;


//    public Integer getCreatedAt() {
//        return createdAt;
//    }

//    public void setCreatedAt(Integer createdAt) {
//        this.createdAt = createdAt;
//    }

//    public Integer getUpdatedAt() {
//        return updatedAt;
//    }

//    public void setUpdatedAt(Integer updatedAt) {
//        this.updatedAt = updatedAt;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(String familyCode) {
        this.familyCode = familyCode;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getSan() {
        return san;
    }

    public void setSan(String san) {
        this.san = san;
    }

    public Integer getVerify() {
        return verify;
    }

    public void setVerify(Integer verify) {
        this.verify = verify;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Wall getWall() {
        return wall;
    }

    public void setWall(Wall wall) {
        this.wall = wall;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getDeceased() {
        return deceased;
    }

    public void setDeceased(String deceased) {
        this.deceased = deceased;
    }

    public String getReportCount() {
        return reportCount;
    }

    public void setReportCount(String reportCount) {
        this.reportCount = reportCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isCanmessage() {
        return canmessage;
    }

    public void setCanmessage(boolean canmessage) {
        this.canmessage = canmessage;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getFcount() {
        return fcount;
    }

    public void setFcount(String fcount) {
        this.fcount = fcount;
    }


    public class Wall {
        @SerializedName("pages")
        @Expose
        private String pages;

        @SerializedName("posts")
        @Expose
        private ArrayList<GetPostPagination.Data.PostDetails> posts;

        public ArrayList<GetPostPagination.Data.PostDetails> getPosts() {
            return posts;
        }

        public void setPosts(ArrayList<GetPostPagination.Data.PostDetails> posts) {
            this.posts = posts;
        }

        public String getPages() {
            return pages;
        }

        public void setPages(String pages) {
            this.pages = pages;
        }
    }
}
