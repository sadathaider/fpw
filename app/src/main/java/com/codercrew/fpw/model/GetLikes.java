
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetLikes {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<LikesDetails> data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LikesDetails> getData() {
        return data;
    }

    public void setData(ArrayList<LikesDetails> data) {
        this.data = data;
    }

    public class LikesDetails {
        @SerializedName("createdAt")
        @Expose
        private String  createdAt;

        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("likerid")
        @Expose
        private String likerid;

        @SerializedName("likername")
        @Expose
        private String likername;

        @SerializedName("likerprofilepic")
        @Expose
        private String likerprofilepic;



        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLikerid() {
            return likerid;
        }

        public void setLikerid(String likerid) {
            this.likerid = likerid;
        }

        public String getLikername() {
            return likername;
        }

        public void setLikername(String likername) {
            this.likername = likername;
        }

        public String getLikerprofilepic() {
            return likerprofilepic;
        }

        public void setLikerprofilepic(String likerprofilepic) {
            this.likerprofilepic = likerprofilepic;
        }
    }

}
