
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetComments {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<CommentsDetails> data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CommentsDetails> getData() {
        return data;
    }

    public void setData(ArrayList<CommentsDetails> data) {
        this.data = data;
    }

    public class CommentsDetails {
        @SerializedName("createdAt")
        @Expose
        private String  createdAt;

        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("postid")
        @Expose
        private String postid;

        @SerializedName("commenterid")
        @Expose
        private String commenterid;

        @SerializedName("commentername")
        @Expose
        private String commentername;

        @SerializedName("comment")
        @Expose
        private String comment;

        @SerializedName("commenterprofilepic")
        @Expose
        private String commenterprofilepic;



        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPostid() {
            return postid;
        }

        public void setPostid(String postid) {
            this.postid = postid;
        }

        public String getCommenterid() {
            return commenterid;
        }

        public void setCommenterid(String commenterid) {
            this.commenterid = commenterid;
        }

        public String getCommentername() {
            return commentername;
        }

        public void setCommentername(String commentername) {
            this.commentername = commentername;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCommenterprofilepic() {
            return commenterprofilepic;
        }

        public void setCommenterprofilepic(String commenterprofilepic) {
            this.commenterprofilepic = commenterprofilepic;
        }
    }

}
