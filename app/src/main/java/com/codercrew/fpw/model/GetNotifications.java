
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetNotifications {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<notificationsDetails> data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<notificationsDetails> getData() {
        return data;
    }

    public void setData(ArrayList<notificationsDetails> data) {
        this.data = data;
    }

    public class notificationsDetails {
        @SerializedName("createdAt")
        @Expose
        private String  createdAt;

        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("post_id")
        @Expose
        private String postid;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("read")
        @Expose
        private String read;

        @SerializedName("familyCode")
        @Expose
        private String familyCode;

        @SerializedName("text")
        @Expose
        private String text;

        @SerializedName("verb")
        @Expose
        private String verb;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("pic")
        @Expose
        private String pic;



        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPostid() {
            return postid;
        }

        public void setPostid(String postid) {
            this.postid = postid;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getRead() {
            return read;
        }

        public void setRead(String read) {
            this.read = read;
        }

        public String getFamilyCode() {
            return familyCode;
        }

        public void setFamilyCode(String familyCode) {
            this.familyCode = familyCode;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getVerb() {
            return verb;
        }

        public void setVerb(String verb) {
            this.verb = verb;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
