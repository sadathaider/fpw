
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetUserMedia {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{
        @SerializedName("image")
        @Expose
        private ArrayList<Image>  images;

        @SerializedName("video")
        @Expose
        private ArrayList<Video>  videos;

        public ArrayList<Image> getImages() {
            return images;
        }

        public void setImages(ArrayList<Image> images) {
            this.images = images;
        }

        public ArrayList<Video> getVideos() {
            return videos;
        }

        public void setVideos(ArrayList<Video> videos) {
            this.videos = videos;
        }


        public class Image {
            @SerializedName("createdAt")
            @Expose
            private String  createdAt;

            @SerializedName("updatedAt")
            @Expose
            private String updatedAt;

            @SerializedName("id")
            @Expose
            private String id;

            @SerializedName("albumid")
            @Expose
            private String albumid;

            @SerializedName("image")
            @Expose
            private String image;

            @SerializedName("userid")
            @Expose
            private String userid;



            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAlbumid() {
                return albumid;
            }

            public void setAlbumid(String albumid) {
                this.albumid = albumid;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }
        }

        public class Video {
            @SerializedName("createdAt")
            @Expose
            private String  createdAt;

            @SerializedName("updatedAt")
            @Expose
            private String updatedAt;

            @SerializedName("id")
            @Expose
            private String id;

            @SerializedName("albumid")
            @Expose
            private String albumid;

            @SerializedName("video")
            @Expose
            private String video;

            @SerializedName("thumb")
            @Expose
            private String thumb;

            @SerializedName("userid")
            @Expose
            private String userid;



            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAlbumid() {
                return albumid;
            }

            public void setAlbumid(String albumid) {
                this.albumid = albumid;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }
        }

    }


}
