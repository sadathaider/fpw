package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.model.Fpassword;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
    ConstraintLayout constraintLayout;
    EditText email;
    Button send;
    TextView backLoginBtn;
    ProgressDialog pd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        constraintLayout = findViewById(R.id.constraintForgotScreen);
        email = findViewById(R.id.emailAddressForgot);
        send = findViewById(R.id.sendBtn);
        backLoginBtn = findViewById(R.id.loginBtnToSend);
        pd =new ProgressDialog(ForgotPasswordActivity.this);
        pd.setMessage(getString(R.string.loading));


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidations();
            }
        });

        backLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
//                startActivity(intent);
                finish();
            }
        });



        constraintLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

//        email.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                String str = s.toString();
//                if(str.length() > 0 && str.contains(" "))
//                {
//                    email.setError("Space is not allowed");
//                    email.setText(s.toString().trim());
//                }
//
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }
    private void SetValidations() {
        if(!utils.CheckInternet(getApplicationContext())){
            Toast.makeText(this, "Check your internet connection!", Toast.LENGTH_SHORT).show();
            return;
        }
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+[ ]*";
        //Email Validations
        if(email.getText().toString().isEmpty()) {
            email.setError("enter email address");
            email.requestFocus();
            return;
        }
        else if(!email.getText().toString().matches(emailPattern)){
            email.setError("Invalid email address");
            email.requestFocus();
            return;
        }

        if(email.getText().toString().matches(emailPattern))
        {
            hitLoginApi();
        }
    }
    private void hitLoginApi() {
        pd.show();
        Call<Fpassword> forgotPassword;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        forgotPassword = apiInterface.forgot_password(email.getText().toString().trim());

        forgotPassword.enqueue(new Callback<Fpassword>() {
            @Override
            public void onResponse(Call<Fpassword> call, Response<Fpassword> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    Toast.makeText(ForgotPasswordActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<Fpassword> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ForgotPasswordActivity.this, "fail"+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}