package com.codercrew.fpw.home.ui.message.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.chat.model.User;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyMembersModel;

import java.util.ArrayList;
import java.util.List;

public class UserMsgsAdapter extends RecyclerView.Adapter<UserMsgsAdapter.BlockedFamilyMembersViewHolder> {
    ArrayList<User> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;
    SharedPrefrencesMain sharedPrefrencesMain;

    public UserMsgsAdapter(Context context, ArrayList<User> familyMembersModelList, OnItemClick onItemClick) {
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
        sharedPrefrencesMain = new SharedPrefrencesMain(context);
    }


    @NonNull
    @Override
    public BlockedFamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.user_messages_row_layout, parent, false);
        return new BlockedFamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedFamilyMembersViewHolder holder, int position) {
        holder.textViewName.setText(familyMembersModelList.get(position).name);
        if (familyMembersModelList.get(position).message.text!=null){
            if (familyMembersModelList.get(position).message.text.contains("pdf")) {
                holder.relativeLayout.setVisibility(View.VISIBLE);
                holder.chatImageview.setVisibility(View.VISIBLE);
                holder.chatImageview.setImageResource(R.drawable.ic_baseline_picture_as_pdf);
                holder.textViewMsg.setText("Pdf:");
            } else if (familyMembersModelList.get(position).message.text.contains("jpg")) {
                holder.relativeLayout.setVisibility(View.VISIBLE);
                holder.chatImageview.setImageResource(R.drawable.ic_baseline_image);
                holder.chatImageview.setVisibility(View.VISIBLE);

                holder.textViewMsg.setText("Image:");
            } else if (familyMembersModelList.get(position).message.text.contains("mp4")) {
                holder.relativeLayout.setVisibility(View.VISIBLE);
                holder.chatImageview.setVisibility(View.VISIBLE);
                holder.chatImageview.setImageResource(R.drawable.ic_baseline_videocam);
                holder.textViewMsg.setText("Video:");
            }else {
                holder.relativeLayout.setVisibility(View.VISIBLE);
                holder.chatImageview.setVisibility(View.GONE);
                holder.textViewMsg.setText(familyMembersModelList.get(position).message.text);

            }
        }else {
            holder.relativeLayout.setVisibility(View.GONE);

        }


        if (familyMembersModelList.get(position).type != null && familyMembersModelList.get(position).type.equals("1")) {
            holder.imageViewMenu.setVisibility(View.GONE);

            if (familyMembersModelList.get(position).message.status.equals("0"))
                holder.textViewCount.setVisibility(View.VISIBLE);
            else holder.textViewCount.setVisibility(View.GONE);

            if (familyMembersModelList.get(position).avata != null && familyMembersModelList.get(position).avata.length() > 0)
                Glide.with(context).load(familyMembersModelList.get(position).avata).into(holder.imageViewProfile);
            else Glide.with(context).load(R.drawable.user_profile).into(holder.imageViewProfile);
        } else {
            holder.textViewCount.setVisibility(View.GONE);
            if (familyMembersModelList.get(position).id.equals(sharedPrefrencesMain.getId())) {
                holder.imageViewMenu.setVisibility(View.VISIBLE);
            } else holder.imageViewMenu.setVisibility(View.GONE);

            Glide.with(context).load(R.drawable.group_icon).into(holder.imageViewProfile);
        }


        holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v, position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class BlockedFamilyMembersViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageViewProfile, imageViewMenu, chatImageview;
        private TextView textViewName, textViewMsg;
        private ConstraintLayout constraintLayout;
        private TextView textViewCount;
        private RelativeLayout relativeLayout;


        public BlockedFamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.tvName);
            textViewMsg = itemView.findViewById(R.id.tvMsg);
            relativeLayout = itemView.findViewById(R.id.rel_chat);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
            chatImageview = itemView.findViewById(R.id.chat_image);
            constraintLayout = itemView.findViewById(R.id.constraintLayout);
            textViewCount = itemView.findViewById(R.id.tvCount);
        }
    }
}
