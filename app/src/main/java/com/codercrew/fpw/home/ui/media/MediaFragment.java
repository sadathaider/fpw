package com.codercrew.fpw.home.ui.media;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.codercrew.fpw.AlbumsActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.media.adapter.MediaPhotosAdapter;
import com.codercrew.fpw.home.ui.media.adapter.MediaVideoAdapter;
import com.codercrew.fpw.model.GetMedia;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class MediaFragment extends Fragment implements OnItemClick {

    private MediaViewModel mViewModel;
    private MediaPhotosAdapter mediaPhotosAdapter;
    protected MediaVideoAdapter mediaVideoAdapter;
    ProgressDialog pd;
    ImageView imageViewPhoto,imageViewVideo;
    RelativeLayout relativeLayoutPhoto,relativeLayoutVideo;
    public static boolean ISMEDIAUPDATED = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.media_fragment, container, false);

        mViewModel = new ViewModelProvider(this).get(MediaViewModel.class);
        // TODO: Use the ViewModel


        final RecyclerView recyclerView = root.findViewById(R.id.rvPhotos);
        final RecyclerView recyclerViewVideos = root.findViewById(R.id.rvVideos);
        imageViewPhoto = root.findViewById(R.id.imgPhoto);
        imageViewVideo = root.findViewById(R.id.imgVideo);
        relativeLayoutPhoto = root.findViewById(R.id.relPhoto);
        relativeLayoutVideo = root.findViewById(R.id.relVideo);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        GridLayoutManager gridLayoutManagerVideo = new GridLayoutManager(getActivity(),3);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerViewVideos.setLayoutManager(gridLayoutManagerVideo);
        mViewModel.getText().observe(getViewLifecycleOwner(), new Observer<GetMedia.Data>() {
            @Override
            public void onChanged(@Nullable GetMedia.Data familyMembersModel) {
                pd.dismiss();
                if((familyMembersModel!=null)) {
                    if(familyMembersModel.getImages()!=null && familyMembersModel.getImages().size()>5) {
                        mediaPhotosAdapter = new MediaPhotosAdapter(getContext(), familyMembersModel, MediaFragment.this);
                        recyclerView.setAdapter(mediaPhotosAdapter);
                    }else {
                        recyclerView.setVisibility(View.GONE);
                        imageViewPhoto.setVisibility(View.VISIBLE);
                        if(familyMembersModel.getImages().size()>0){
                            imageViewPhoto.setVisibility(View.VISIBLE);
                            Glide.with(getActivity()).load(familyMembersModel.getImages().get(0).getImage()).into(imageViewPhoto);
                        }
                    }
                    if(familyMembersModel.getVideos()!=null && familyMembersModel.getVideos().size()>5) {
                        mediaVideoAdapter = new MediaVideoAdapter(getContext(), familyMembersModel, MediaFragment.this);
                        recyclerViewVideos.setAdapter(mediaVideoAdapter);
                    }else {
                        recyclerViewVideos.setVisibility(View.GONE);
                        imageViewVideo.setVisibility(View.VISIBLE);
                        if(familyMembersModel.getVideos().size()>0){
                            imageViewVideo.setVisibility(View.VISIBLE);
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.isMemoryCacheable();
                            Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(familyMembersModel.getVideos().get(0).getThumb()).into(imageViewVideo);
                        }
                    }
                }
            }
        });

        relativeLayoutVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AlbumsActivity.class);
                intent.putExtra("type","2");
                startActivity(intent);
            }
        });
        relativeLayoutPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AlbumsActivity.class);
                intent.putExtra("type","1");
                startActivity(intent);
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View view, int pos) {

    }

    @Override
    public void onResume(){
        super.onResume();
        pd =new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();
        mViewModel = new ViewModelProvider(this).get(MediaViewModel.class);
        if(ISMEDIAUPDATED) {
            ISMEDIAUPDATED = false;
            pd.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mViewModel.hitGetMediaApi("111");
                }
            }, 3000);
        }else mViewModel.hitGetMediaApi("111");

    }
}