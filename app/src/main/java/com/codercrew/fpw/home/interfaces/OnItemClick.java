package com.codercrew.fpw.home.interfaces;

import android.view.View;

public interface OnItemClick {
    public void onClick(View view, int pos);
}
