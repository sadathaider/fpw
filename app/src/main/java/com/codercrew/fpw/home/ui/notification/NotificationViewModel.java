package com.codercrew.fpw.home.ui.notification;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetNotifications;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationViewModel extends ViewModel {

    private MutableLiveData<ArrayList<GetNotifications.notificationsDetails>> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public NotificationViewModel() {
        mText = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
    }

    public LiveData<ArrayList<GetNotifications.notificationsDetails>> getText() {
        return mText;
    }


    public void hitGetNotificationApi(String pid)
    {
        Call<GetNotifications> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getnotifications("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetNotifications>() {
            @Override
            public void onResponse(Call<GetNotifications> call, Response<GetNotifications> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetNotifications> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }


}