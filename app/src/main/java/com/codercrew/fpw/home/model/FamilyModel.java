package com.codercrew.fpw.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FamilyModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("familyCode")
    @Expose
    private String familyCode;
    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("status")
    @Expose
    private String status;

    public FamilyModel(String id, String familyCode, String family, String status){
        this.id = id;
        this.familyCode = familyCode;
        this.family = family;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(String name) {
        this.familyCode = name;
    }

    public String getFamily() {
        return family;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
