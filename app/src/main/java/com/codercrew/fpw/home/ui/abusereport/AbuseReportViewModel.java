package com.codercrew.fpw.home.ui.abusereport;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.SignUp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AbuseReportViewModel extends AndroidViewModel {
    private MutableLiveData<SignUp> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;
    private Context context;
    private SignUp model;
    // TODO: Implement the ViewModel

    public AbuseReportViewModel(Application application) {
        super(application);
        context = application.getApplicationContext();
        mText = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
//        hitProfileApi();
    }

    public LiveData<SignUp> getText() {
        return mText;
    }


    public void hitProfileApi()
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getProfile("Bearer "+sharedPrefrencesMain.getToken(),"");

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
                    model = response.body();
                    mText.setValue(model);
//                    return model;
                }else {
                    Toast.makeText(context, ""+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    mText.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}