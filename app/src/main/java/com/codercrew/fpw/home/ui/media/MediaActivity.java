package com.codercrew.fpw.home.ui.media;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.codercrew.fpw.NewAlbumsActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.media.adapter.MediaPhotosAdapter;
import com.codercrew.fpw.home.ui.media.adapter.MediaVideoAdapter;
import com.codercrew.fpw.model.GetMedia;

public class MediaActivity extends AppCompatActivity implements OnItemClick {

    private MediaViewModel mViewModel;
    private MediaPhotosAdapter mediaPhotosAdapter;
    protected MediaVideoAdapter mediaVideoAdapter;
    ProgressDialog pd;
    ImageView imageViewPhoto,imageViewVideo;
    RelativeLayout relativeLayoutPhoto,relativeLayoutVideo;
    String m_id;
    public static boolean ISMEDIAUPDATED = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_fragment);
        mViewModel = new ViewModelProvider(this).get(MediaViewModel.class);
        m_id=getIntent().getStringExtra("user_id");
        final RecyclerView recyclerView = findViewById(R.id.rvPhotos);
        final RecyclerView recyclerViewVideos = findViewById(R.id.rvVideos);
        imageViewPhoto = findViewById(R.id.imgPhoto);
        imageViewVideo = findViewById(R.id.imgVideo);
        relativeLayoutPhoto =  findViewById(R.id.relPhoto);
        relativeLayoutVideo =  findViewById(R.id.relVideo);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MediaActivity.this,3);
        GridLayoutManager gridLayoutManagerVideo = new GridLayoutManager(MediaActivity.this,3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerViewVideos.setLayoutManager(gridLayoutManagerVideo);
        mViewModel.getText().observe(this, new Observer<GetMedia.Data>() {
            @Override
            public void onChanged(@Nullable GetMedia.Data familyMembersModel) {
                pd.dismiss();
                if((familyMembersModel!=null)) {
                    if(familyMembersModel.getImages()!=null && familyMembersModel.getImages().size()>5) {
                        mediaPhotosAdapter = new MediaPhotosAdapter(MediaActivity.this, familyMembersModel, MediaActivity.this);
                        recyclerView.setAdapter(mediaPhotosAdapter);
                    }else {
                        recyclerView.setVisibility(View.GONE);
                        imageViewPhoto.setVisibility(View.VISIBLE);
                        if(familyMembersModel.getImages().size()>0){
                            imageViewPhoto.setVisibility(View.VISIBLE);
                            Glide.with(MediaActivity.this).load(familyMembersModel.getImages().get(0).getImage()).into(imageViewPhoto);
                        }
                    }
                    if(familyMembersModel.getVideos()!=null && familyMembersModel.getVideos().size()>5) {
                        mediaVideoAdapter = new MediaVideoAdapter(MediaActivity.this, familyMembersModel, MediaActivity.this);
                        recyclerViewVideos.setAdapter(mediaVideoAdapter);
                    }else {
                        recyclerViewVideos.setVisibility(View.GONE);
                        imageViewVideo.setVisibility(View.VISIBLE);
                        if(familyMembersModel.getVideos().size()>0){
                            imageViewVideo.setVisibility(View.VISIBLE);
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.isMemoryCacheable();
                            Glide.with(MediaActivity.this).setDefaultRequestOptions(requestOptions).load(familyMembersModel.getVideos().get(0).getThumb()).into(imageViewVideo);
                        }
                    }
                }
            }
        });

        relativeLayoutVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaActivity.this, NewAlbumsActivity.class);
                intent.putExtra("type","2");
                intent.putExtra("m_id",m_id);
                startActivity(intent);
            }
        });
        relativeLayoutPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaActivity.this, NewAlbumsActivity.class);
                intent.putExtra("type","1");
                intent.putExtra("m_id",m_id);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onClick(View view, int pos) {

    }

    @Override
    public void onResume(){
        super.onResume();
        pd =new ProgressDialog(MediaActivity.this);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();
        mViewModel = new ViewModelProvider(this).get(MediaViewModel.class);
//        if(ISMEDIAUPDATED) {
//            ISMEDIAUPDATED = false;
//            pd.show();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mViewModel.hitGetMediaApi("111");
//                }
//            }, 3000);
//        }
//        else{
//            mViewModel.hitGetMediaApi("111");
            mViewModel.hitGetMediaOfUserApi(m_id);
//        }

    }
}