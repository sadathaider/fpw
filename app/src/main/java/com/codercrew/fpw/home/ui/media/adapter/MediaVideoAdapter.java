package com.codercrew.fpw.home.ui.media.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetMedia;
import com.bumptech.glide.Glide;

public class MediaVideoAdapter extends RecyclerView.Adapter<MediaVideoAdapter.BlockedFamilyMembersViewHolder> {

    GetMedia.Data familyMembersModelList;// = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public MediaVideoAdapter(Context context, GetMedia.Data familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public BlockedFamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.media_row_layout,parent,false);
        return new BlockedFamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedFamilyMembersViewHolder holder, int position) {

        Glide.with(context).load(familyMembersModelList.getVideos().get(position).getThumb()).into(holder.imageViewProfile);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.getVideos().size();
    }

    public class BlockedFamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;


        public BlockedFamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
        }
    }
}
