package com.codercrew.fpw.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.AlbumGalleryActivity;
import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.EditProfileActivity;
import com.codercrew.fpw.MessagesActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.SplashScreen;
import com.codercrew.fpw.Utils.Config;
import com.codercrew.fpw.Utils.NotificationUtils;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.bumptech.glide.Glide;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.ViewPostNewActivity;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.SignUp;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.home.ui.subscription.PaymentSubscriptionFragment.LOAD_PAYMENT_DATA_REQUEST_CODE;
import static com.codercrew.fpw.home.ui.subscription.PaymentSubscriptionFragment.plan;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private SharedPrefrencesMain sharedPrefrencesMain;
    private TextView textView,textViewTitle;
    private ImageView imageViewEdit,imageViewAdd;
    static ImageView imageView;
    public static MainActivity mainActivity;
    BroadcastReceiver broadcastReceiver;
    private ImageView imageViewDrawer,imageViewTitle;
    DrawerLayout drawer;
    NavigationView navigationView;
    NavController navController;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainActivity = this;
        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        FloatingActionButton fab = findViewById(R.id.fab);
        imageViewDrawer = findViewById(R.id.ivDrawer);
        textViewTitle = findViewById(R.id.tvTitle);
        imageViewTitle = findViewById(R.id.ivTitle);
        imageViewEdit = findViewById(R.id.imgEdit);
        imageViewAdd = findViewById(R.id.imgAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_profile, R.id.nav_logout, R.id.nav_wall,R.id.nav_setting,R.id.nav_notifications,R.id.nav_subscription,R.id.nav_messages,R.id.nav_abuse,R.id.nav_aboutus)
                .setDrawerLayout(drawer)
                .build();

        View headerView = navigationView.getHeaderView(0);
        textView = headerView.findViewById(R.id.textView);
        imageView = headerView.findViewById(R.id.imageView);

        navigationView.setItemIconTintList(null);





        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.navigate(R.id.nav_profile);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                ColorStateList csl = AppCompatResources.getColorStateList(getApplicationContext(), R.color.white);
                if(destination.getId()==R.id.nav_profile){
                    textViewTitle.setText(R.string.menu_profile);
                    imageViewTitle.setImageResource(R.drawable.profile);
                    imageViewEdit.setVisibility(View.VISIBLE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_setting){
                    textViewTitle.setText(R.string.menu_settings);
                    imageViewTitle.setImageResource(R.drawable.setting);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_wall){
                    id = R.id.nav_wall;
                    textViewTitle.setText(R.string.menu_walls);
                    imageViewTitle.setImageResource(R.drawable.wall);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_media){
                    textViewTitle.setText(R.string.menu_media);
                    imageViewTitle.setImageResource(R.drawable.gallery);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_newsfeed){
                    id = R.id.nav_newsfeed;
                    textViewTitle.setText(R.string.menu_newsfeed);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    imageViewTitle.setImageResource(R.drawable.newsfeed);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(null);
                    }
                }else if(destination.getId()==R.id.nav_logout){
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if(sharedPrefrencesMain.Logout()){
                        Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                        startActivity(intent);
                        finish();
                    }
                }else if(destination.getId()==R.id.nav_messages){
                    textViewTitle.setText(R.string.menu_messages);
                    imageViewTitle.setImageResource(R.drawable.messages);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_notifications){
                    textViewTitle.setText(R.string.menu_notifications);
                    imageViewTitle.setImageResource(R.drawable.notification);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_abuse){
                    textViewTitle.setText(R.string.menu_abuse);
                    imageViewTitle.setImageResource(R.drawable.reporticon);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_aboutus){
                    textViewTitle.setText(R.string.menu_aboutus);
                    imageViewTitle.setImageResource(R.drawable.ic_outline_info_24);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }else if(destination.getId()==R.id.nav_subscription){
                    textViewTitle.setText(R.string.menu_subscription);
                    imageViewTitle.setImageResource(R.drawable.paymeticon);
                    imageViewEdit.setVisibility(View.GONE);
                    imageViewAdd.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        imageViewTitle.setImageTintList(csl);
                    }
                }
            }
        });

        imageViewDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(drawer.isOpen()){
                    drawer.close();
                }else {
                    drawer.open();
                }
            }
        });

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        imageViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
                startActivity(intent);
            }
        });

        if(getIntent().getExtras()!=null) {
            String message = getIntent().getStringExtra("message");
            String title = getIntent().getStringExtra("title");
            String body = getIntent().getStringExtra("body");
            showBirthdayDialog(title,message,body);
        }else {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // checking for type intent filter
                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                        //   displayFirebaseRegId();

                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        // new push notification is received
                        String message = intent.getStringExtra("message");
                        String title = intent.getStringExtra("title");
                        String body = intent.getStringExtra("body");
                        showBirthdayDialog(title, message, body);
//                    Toast.makeText(context, ""+body, Toast.LENGTH_SHORT).show();
                    }
                }
            };
        }


        FirebaseMessaging.getInstance().subscribeToTopic("news");

    }

    public void setMenu(){
//        navController.navigate(R.id.nav_profile);
        navController.popBackStack();
//        ColorStateList csl = AppCompatResources.getColorStateList(getApplicationContext(), R.color.white);
//        textViewTitle.setText(R.string.menu_profile);
//        imageViewTitle.setImageResource(R.drawable.profile);
//        imageViewEdit.setVisibility(View.VISIBLE);
//        imageViewAdd.setVisibility(View.GONE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            imageViewTitle.setImageTintList(csl);
//        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public static void SetProfile(String url){
        if(url.length()>0)
            try {
                Glide.with(mainActivity).load(url).into(imageView);
            }catch (IllegalArgumentException i){
                i.getMessage();
            }

    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onResume(){
        super.onResume();
        textView.setText(sharedPrefrencesMain.getName());

        if(sharedPrefrencesMain.getProfileUrl().length()>0)
            Glide.with(this).load(sharedPrefrencesMain.getProfileUrl()).into(imageView);

        if(utils.count>0) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            String menutitle = "Messages ";
            SpannableString blackSpannable = new SpannableString(menutitle);
            blackSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, menutitle.length(), 0);
            builder.append(blackSpannable);

            String count = "(" + utils.count + ")";
            SpannableString redSpannable = new SpannableString(count);
            redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, count.length(), 0);
            builder.append(redSpannable);
            navigationView.getMenu().getItem(4).setTitle(builder);

        }else
            navigationView.getMenu().getItem(4).setTitle("Messages");
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }
    @Override
    public void onBackPressed() {
        if(drawer.isOpen()){
            drawer.close();
        }else
            super.onBackPressed();
//            finish();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    private void showBirthdayDialog(String title,String msg,String body){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MainActivity.this);

        builder1.setTitle(title);
        builder1.setMessage(msg);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Wish",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showWishDialog(body);
//                        arrayList.remove(pos);
                    }
                });
        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    private void showWishDialog(String uid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MainActivity.this);

        builder1.setTitle("Birthday Wish");
//        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);
        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setHint("Enter Your Wish");
        input.requestFocus();
        input.setLayoutParams(lp);
        builder1.setView(input);
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(input.getText().toString().isEmpty()){
                            input.setError("Enter Wish");
                            input.requestFocus();
                            Toast.makeText(getApplicationContext(),
                                    "Wish Required!", Toast.LENGTH_SHORT).show();
                        }else {
                           hitUpdateTokenApi(sharedPrefrencesMain.getToken(),input.getText().toString(),sharedPrefrencesMain.getFmCode(),uid);
                        }
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    public void hitUpdateTokenApi(String authToken,String text,String fCode,String uid)
    {
        Call<GetPost> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.birthday("Bearer "+authToken,text,fCode,uid);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if(response.isSuccessful()){
                    Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else  Toast.makeText(MainActivity.this, ""+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // value passed in AutoResolveHelper
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_PAYMENT_DATA_REQUEST_CODE) {
            switch (resultCode) {

                case Activity.RESULT_OK:
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    handlePaymentSuccess(paymentData);
                    break;

                case Activity.RESULT_CANCELED:
                    // The user cancelled the payment attempt
                    break;

                case AutoResolveHelper.RESULT_ERROR:
                    Status status = AutoResolveHelper.getStatusFromIntent(data);
                    handleError(status.getStatusCode());
                    break;
            }
        } else {
            if(data!=null) {
                NavHostFragment navHostFragment = (NavHostFragment)getSupportFragmentManager().getPrimaryNavigationFragment();
                FragmentManager fragmentManager = navHostFragment.getChildFragmentManager();
                Fragment loginFragment = fragmentManager.getPrimaryNavigationFragment();
//                Fragment fragment = getSupportFragmentManager().findFragmentById(navController.getCurrentDestination().getId());
                loginFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void handlePaymentSuccess(PaymentData paymentData) {

        // Token will be null if PaymentDataRequest was not constructed using fromJson(String).
        final String paymentInfo = paymentData.toJson();
        if (paymentInfo == null) {
            return;
        }

        try {
            JSONObject paymentMethodData = new JSONObject(paymentInfo).getJSONObject("paymentMethodData");
            // If the gateway is set to "example", no payment information is returned - instead, the
            // token will only consist of "examplePaymentMethodToken".

            final JSONObject tokenizationData = paymentMethodData.getJSONObject("tokenizationData");
            final String tokenizationType = tokenizationData.getString("type");
            final String token = tokenizationData.getString("token");

//            if ("PAYMENT_GATEWAY".equals(tokenizationType) && "examplePaymentMethodToken".equals(token)) {
//                new AlertDialog.Builder(MainActivity.this)
//                        .setTitle("Warning")
//                        .setMessage(getString(R.string.gateway_replace_name_example))
//                        .setPositiveButton("OK", null)
//                        .create()
//                        .show();
//            }

            final JSONObject info = paymentMethodData.getJSONObject("info");
            final String billingName = "";//info.getJSONObject("billingAddress").getString("name");
            Toast.makeText(
                    MainActivity.this, getString(R.string.payments_show_name, billingName),
                    Toast.LENGTH_LONG).show();
            if(plan.equals("1"))
                hitPurchasePlanApi(plan,paymentMethodData.toString(),"13.99");
            if(plan.equals("2"))
                hitPurchasePlanApi(plan,paymentMethodData.toString(),"26.99");
            if(plan.equals("3"))
                hitPurchasePlanApi(plan,paymentMethodData.toString(),"42.99");
            // Logging token string.
            Log.d("Google Pay token: ", token);

        } catch (JSONException e) {
            throw new RuntimeException("The selected garment cannot be parsed from the list of elements");
        }
    }



    private void handleError(int statusCode) {
        Log.e("loadPaymentData failed", String.format("Error code: %d", statusCode));
    }

    public void hitPurchasePlanApi(String plan,String payment,String amount)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.purchaseplan("Bearer "+sharedPrefrencesMain.getToken(),plan,payment,amount);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}