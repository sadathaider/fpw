package com.codercrew.fpw.home.ui.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.User;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class FamilyMembersAdapter extends RecyclerView.Adapter<FamilyMembersAdapter.FamilyMembersViewHolder> {

    List<User> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;
    boolean isMember;

    public FamilyMembersAdapter(Context context, List<User> familyMembersModelList,boolean isMember, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
        this.isMember = isMember;
    }


    @NonNull
    @Override
    public FamilyMembersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.family_members_row_layout,parent,false);
        return new FamilyMembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyMembersViewHolder holder, int position) {
        if(isMember) {
            holder.imageViewMenu.setVisibility(View.GONE);
            if(familyMembersModelList.get(position).getRelation().length()>0)
                holder.textViewRel.setText("("+familyMembersModelList.get(position).getRelation()+")");
            else holder.textViewRel.setText("");
        }
        holder.imageViewProfile.setImageResource(R.drawable.user_profile);
        holder.textViewName.setText(familyMembersModelList.get(position).getName());
        if(familyMembersModelList.get(position).getProfilepic().length()>0)
            Glide.with(AppContext.getAppContext()).load(familyMembersModelList.get(position).getProfilepic()).into(holder.imageViewProfile);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
        holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class FamilyMembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName,textViewRel;

        public FamilyMembersViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
            textViewName = itemView.findViewById(R.id.tvName);
            textViewRel = itemView.findViewById(R.id.tvRel);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
        }
    }
}
