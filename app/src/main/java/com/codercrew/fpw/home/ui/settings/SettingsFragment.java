package com.codercrew.fpw.home.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.codercrew.fpw.BlockedMembersActivity;
import com.codercrew.fpw.ChangePasswordActivity;
import com.codercrew.fpw.ContactUsActivity;
import com.codercrew.fpw.FamilySettingActivity;
import com.codercrew.fpw.R;


public class SettingsFragment extends Fragment implements View.OnClickListener{

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        final LinearLayout lnChangePswd = root.findViewById(R.id.lnChangePswd);
        final LinearLayout lnFamilies = root.findViewById(R.id.lnFamilies);
        final LinearLayout lnBloclMember = root.findViewById(R.id.lnBloclMember);
        final LinearLayout lnContactus = root.findViewById(R.id.lnContactus);
        lnChangePswd.setOnClickListener(this);
        lnFamilies.setOnClickListener(this);
        lnBloclMember.setOnClickListener(this);
        lnContactus.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.lnChangePswd){
            Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
            startActivity(intent);
        }else if(v.getId()==R.id.lnFamilies){
            Intent intent = new Intent(getContext(), FamilySettingActivity.class);
            startActivity(intent);
        }else if(v.getId()==R.id.lnBloclMember){
            Intent intent = new Intent(getContext(), BlockedMembersActivity.class);
            startActivity(intent);
        }else if(v.getId()==R.id.lnContactus){
            Intent intent = new Intent(getContext(), ContactUsActivity.class);
            startActivity(intent);
        }
    }
}