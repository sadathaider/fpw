package com.codercrew.fpw.home.ui.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.EditProfileActivity;
import com.codercrew.fpw.MemberProfileWalls;
import com.codercrew.fpw.home.ui.media.MediaActivity;
import com.codercrew.fpw.MemberProfileActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.SplashScreen;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.chat.ChatActivity;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.profile.adapter.FamilyMembersAdapter;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.home.MainActivity.SetProfile;

public class ProfileFragment extends Fragment implements View.OnClickListener, OnItemClick {
    private ProfileViewModel profileViewModel;
    private FamilyMembersAdapter familyMembersAdapter;
    ProgressDialog pd;
    private List<User> user = new ArrayList<>();
    private SharedPrefrencesMain sharedPrefrencesMain;
    RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        sharedPrefrencesMain = new SharedPrefrencesMain(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setMessage(getActivity().getString(R.string.loading));

        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        recyclerView = root.findViewById(R.id.rvFamilyMembers);
        final ImageView imageViewEdit = root.findViewById(R.id.imgEdit);
        final ImageView imageViewProfile = root.findViewById(R.id.imgProfile);
        final TextView textViewName = root.findViewById(R.id.tvName);
        final TextView textViewBd = root.findViewById(R.id.tvBd);
        final TextView textViewAdd = root.findViewById(R.id.tvAddress);

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        if (sharedPrefrencesMain.getProfileUrl().length() > 0)
            Glide.with(AppContext.getAppContext()).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);
        if (sharedPrefrencesMain.getName().length() > 0) {
            String str = sharedPrefrencesMain.getName();
            String nameCap = str.substring(0, 1).toUpperCase() + str.substring(1);
            textViewName.setText(nameCap);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });
        recyclerView.setOnClickListener(this);
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        if(!pd.isShowing())
            pd.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
            }
        },2000);
        profileViewModel.getText().observe(getViewLifecycleOwner(), new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                pd.cancel();
                if (familyMembersModel != null && familyMembersModel.getData() != null) {
                    pd.dismiss();
                    pd.cancel();
//                    familyMembersModel.getData().getUser().setStatus("1");
                    if (familyMembersModel.getData().getUser().getStatus().equals("1")) {
                        if (sharedPrefrencesMain.Logout()) {
                            Toast.makeText(getContext(), "Banned by admin", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), SplashScreen.class);
                            startActivity(intent);
                            MainActivity.mainActivity.finish();
                            return;
                        }
                    }

                    if (familyMembersModel.getData().getUser().getProfilepic().length() > 0) {
                        SetProfile(familyMembersModel.getData().getUser().getProfilepic());
                        Glide.with(AppContext.getAppContext()).load(familyMembersModel.getData().getUser().getProfilepic()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                pd.dismiss();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                imageViewProfile.setImageResource(R.drawable.user_profile);
                                pd.dismiss();
                                return false;
                            }
                        }).into(imageViewProfile);
                    } else pd.dismiss();
                    sharedPrefrencesMain.setUser(familyMembersModel.getData().getUser());
                    String str = sharedPrefrencesMain.getName();
                    String nameCap = str.substring(0, 1).toUpperCase() + str.substring(1);

                    textViewName.setText(nameCap);
                    textViewBd.setText(familyMembersModel.getData().getUser().getDob());
                    if (familyMembersModel.getData().getUser().getCity().length() > 0)
                        textViewAdd.setText(familyMembersModel.getData().getUser().getCity() + "," + familyMembersModel.getData().getUser().getCountry());
                    if (familyMembersModel.getData().getMembers() != null && familyMembersModel.getData().getMembers().size() > 0) {
                        user.clear();
                        for (User use : familyMembersModel.getData().getMembers()) {
                            if (use.getBlocked().equals("0") && use.getDeceased().equals("0"))
                                user.add(use);
                        }

                        //user = familyMembersModel.getData().getMembers();
                        familyMembersAdapter = new FamilyMembersAdapter(getActivity(), user, false,ProfileFragment.this);
                        recyclerView.setAdapter(familyMembersAdapter);
                    }
                }
            }
        });


        return root;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgEdit) {

            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view, int pos) {
        if (view.getId() == R.id.imgMenu) {
            hitProfileApi(user.get(pos).getId());

        } else {
            Intent intent = new Intent(getActivity(), MemberProfileActivity.class);
            intent.putExtra("user", user.get(pos));
            startActivity(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        profileViewModel.hitProfileApi();
    }

    public void hitProfileApi(String id) {
        if(!pd.isShowing())
            pd.show();;
        Call<GetProfile> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getprofilebyid("Bearer " + sharedPrefrencesMain.getToken(), id);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
// ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = ProfileFragment.this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.custom_menu_family_members, null);
                    dialogBuilder.setView(dialogView);
                    AlertDialog alertDialog = dialogBuilder.create();
                    ImageView imageViewP = dialogView.findViewById(R.id.imgProfile);
                    TextView textViewN = dialogView.findViewById(R.id.tvName);
                    TextView textViewRel = dialogView.findViewById(R.id.tvRel);
                    ImageView imageViewC = dialogView.findViewById(R.id.imgClose);
                    TextView txtChangeRel = dialogView.findViewById(R.id.txtChangeRel);
                    TextView textViewReport = dialogView.findViewById(R.id.txtReport);
                    TextView textViewDecesed = dialogView.findViewById(R.id.txtMark);
                    TextView textViewBlock = dialogView.findViewById(R.id.txtBock);
                    TextView txtSendMessage = dialogView.findViewById(R.id.txtSendMessage);
                    TextView txtPhotoVideo = dialogView.findViewById(R.id.txtPhotoVideo);
                    TextView txtWalls = dialogView.findViewById(R.id.txtWalls);
                    if (response.body().getData().getProfilepic().length() > 0)
                        Glide.with(getActivity()).load(response.body().getData().getProfilepic()).into(imageViewP);
                    textViewN.setText(response.body().getData().getName());
                    textViewRel.setText(response.body().getData().getRelation());
                    imageViewC.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!pd.isShowing())
                                pd.show();;
                            profileViewModel.hitProfileApi();
                            alertDialog.dismiss();
                        }
                    });

                    txtSendMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hitCanMessageApi(response.body().getData().getId(), response.body().getData());
                        }
                    });

                    textViewReport.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AskDialog("Report User " + response.body().getData().getName(), response.body().getData().getId(), 1);
                        }
                    });
                    textViewDecesed.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AskDialog("Mark as Deceased " + response.body().getData().getName(), response.body().getData().getId(), 2);
                        }
                    });
                    textViewBlock.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AskDialog("Block User " + response.body().getData().getName(), response.body().getData().getId(), 3);
                        }
                    });
                    txtPhotoVideo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), MediaActivity.class);
                            intent.putExtra("user_id", response.body().getData().getId());
                            alertDialog.dismiss();
                            getContext().startActivity(intent);
                        }
                    });

                    txtChangeRel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getActivity());
                            builder1.setTitle("Change Relation");
                            builder1.setMessage("Are you sure");
                            builder1.setCancelable(false);
                            final EditText input = new EditText(getActivity());
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            input.setHint("Enter Relation");
                            input.requestFocus();
                            input.setLayoutParams(lp);
                            builder1.setView(input);
                            builder1.setPositiveButton(
                                    "Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            if (input.getText().toString().isEmpty()) {
                                                input.setError("Enter Relation");
                                                input.requestFocus();
                                                Toast.makeText(getContext(),
                                                        "Relation Required!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                if(!pd.isShowing())
                                                    pd.show();
                                                profileViewModel.hitRelationApi(response.body().getData().getEmail(), input.getText().toString());
                                                dialog.cancel();
                                                alertDialog.dismiss();
                                            }
                                        }
                                    });
                            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder1.show();

                        }
                    });

                    imageViewP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), MemberProfileActivity.class);
                            intent.putExtra("user", response.body().getData());
                            startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });

                    txtWalls.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), MemberProfileWalls.class);
                            intent.putExtra("user", response.body().getData());
                            startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });


                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void AskDialog(String title, String uid, int type) {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getActivity());

        builder1.setTitle(title);
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (type == 1)
                            profileViewModel.hitReportApi(uid);
                        else if (type == 2)
                            profileViewModel.hitDecesedApi(uid);
                        else if (type == 3)
                            profileViewModel.hitBlockApi(uid);

                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    public void hitCanMessageApi(String postId, User user) {
        if(!pd.isShowing())
            pd.show();
        Call<GetProfile> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.canmessage("Bearer " + sharedPrefrencesMain.getToken(), postId);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                pd.dismiss();
                if (response.isSuccessful() && response.body().getStatus()) {
//                    Toast.makeText(getApplicationContext(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    if(response.body().getData().isCanmessage()){
                    user.setType("1");
                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra("user", user);
                    intent.putExtra("canmessage", response.body().getData().isCanmessage());
                    intent.putExtra("message", response.body().getMessage());
                    startActivity(intent);
//                    }
                }
            }

            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}