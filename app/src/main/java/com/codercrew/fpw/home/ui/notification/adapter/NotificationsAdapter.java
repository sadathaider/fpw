package com.codercrew.fpw.home.ui.notification.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetNotifications;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.CommentsViewHolder> {

    ArrayList<GetNotifications.notificationsDetails> notificationsDetails = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;
    SharedPrefrencesMain sharedPrefrencesMain;

    public NotificationsAdapter(Context context, ArrayList<GetNotifications.notificationsDetails> notificationsDetails, OnItemClick onItemClick){
        this.context = context;
        this.notificationsDetails = notificationsDetails;
        this.onItemClick = onItemClick;
        sharedPrefrencesMain = new SharedPrefrencesMain(context);
    }


    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.comments_row_layout,parent,false);
        return new CommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {

        holder.imageViewMenu.setVisibility(View.GONE);
        holder.textViewName.setText(notificationsDetails.get(position).getName());
        holder.textViewComment.setText(notificationsDetails.get(position).getText());

        if(!notificationsDetails.get(position).getPic().isEmpty())
            Glide.with(context).load(notificationsDetails.get(position).getPic()).into(holder.imageViewProfile);
        else Glide.with(context).load(R.drawable.user_profile).into(holder.imageViewProfile);
//
//        if(notificationsDetails.get(position).getCommenterid().equals(sharedPrefrencesMain.getId())){
//            holder.imageViewMenu.setVisibility(View.VISIBLE);
//        }
        holder.imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return notificationsDetails.size();
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewMenu;
        private TextView textViewName,textViewComment;


        public CommentsViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewName = itemView.findViewById(R.id.tvName);
            textViewComment = itemView.findViewById(R.id.tvComments);
            imageViewMenu = itemView.findViewById(R.id.imgMenu);
        }
    }
}
