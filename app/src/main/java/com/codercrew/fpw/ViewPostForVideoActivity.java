package com.codercrew.fpw;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetPostPagination;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

public class ViewPostForVideoActivity extends AppCompatActivity implements Player.EventListener {

    PhotoView imageView;
    VideoView videoView;
    PlayerView playerView;
    ProgressDialog pDialog;
    String type;
    String url;
    private SimpleExoPlayer player;
    private TextView editTextCap;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        imageView = findViewById(R.id.imageView);
        videoView = findViewById(R.id.videoView);
        playerView = findViewById(R.id.video_view);
        progressBar = findViewById(R.id.progress);
        editTextCap = findViewById(R.id.tvCaption);

//        playerView.setPlayer(player);

        imageView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        //receive

        if(getIntent().getStringExtra("from").equals("gallery")){
            GetAlbumPhotos.Data model = (GetAlbumPhotos.Data) getIntent().getSerializableExtra("post");
            if(model.getImage()!=null && model.getImage().length()>0){
                type = "1";
                url = model.getImage();
            }else if(model.getVideo()!=null && model.getVideo().length()>0){
                type = "2";
                url = model.getVideo();
                ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,0,0,50);
                editTextCap.setLayoutParams(params);
            }
            if(model.getCaption()!=null && model.getCaption().length()>0){
                editTextCap.setText(model.getCaption());
                editTextCap.setVisibility(View.VISIBLE);
            }
        }else if(getIntent().getStringExtra("from").equals("notification")){
            GetPost.PostDetails model = (GetPost.PostDetails) getIntent().getSerializableExtra("post");
            type = model.getType();
            if(model.getImage()!=null && model.getImage().length()>0){
                url = model.getImage();
            }else if(model.getVideo()!=null && model.getVideo().length()>0){
                url = model.getVideo();
                ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,0,0,50);
                editTextCap.setLayoutParams(params);
            }
            if(model.getCaption()!=null && model.getCaption().length()>0){
                editTextCap.setText(model.getCaption());
                editTextCap.setVisibility(View.VISIBLE);
            }
        }else {
            GetPostPagination.Data.PostDetails model = (GetPostPagination.Data.PostDetails) getIntent().getSerializableExtra("post");
            type = model.getType();
            if(model.getImage()!=null && model.getImage().length()>0){
                url = model.getImage();
            }else if(model.getVideo()!=null && model.getVideo().length()>0){
                url = model.getVideo();

            }
            if(model.getCaption()!=null && model.getCaption().length()>0){
                editTextCap.setText(model.getCaption());
                editTextCap.setVisibility(View.VISIBLE);
            }
        }

        if(type.equals("1")){
            imageView.setVisibility(View.VISIBLE);
            Glide.with(this).load(url).into(imageView);
        }else if(type.equals("2")){

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            playerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            TrackSelector trackSelectorDef = new DefaultTrackSelector();
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef); //creating a player instance

            String userAgent = Util.getUserAgent(this, "");
            @SuppressLint("RestrictedApi") DefaultDataSourceFactory defdataSourceFactory = new DefaultDataSourceFactory(this,userAgent);
            Uri uriOfContentUrl = Uri.parse(url);
            MediaSource mediaSource = new ProgressiveMediaSource.Factory(defdataSourceFactory).createMediaSource(uriOfContentUrl);  // creating a media source

            player.prepare(mediaSource);
            player.setPlayWhenReady(true); // start loading video and play it at the moment a chunk of it is available offline

            playerView.setPlayer(player); // attach surface to the view
            player.addListener(new Player.EventListener() {
                @Override
                public void onPlayerStateChanged(boolean b,int i){

                    if(i==Player.STATE_READY){
                        progressBar.setVisibility(View.GONE);
                    }else if(i==Player.STATE_ENDED){
                        finish();
                    }else if(i==Player.STATE_BUFFERING){
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onPlayerError(ExoPlaybackException exo){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ViewPostForVideoActivity.this, ""+exo.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if(player!=null)
            player.release();
    }

}