package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.codercrew.fpw.model.GetAlbums;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAlbumViewModel extends ViewModel {

    private MutableLiveData<ArrayList<GetAlbums.Data>> mText;
    private MutableLiveData<ArrayList<GetAlbumPhotos.Data>> mAdd;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public AddAlbumViewModel() {
        mText = new MutableLiveData<>();
        mAdd = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());

    }

    public LiveData<ArrayList<GetAlbums.Data>> getText() {
        return mText;
    }

    public LiveData<ArrayList<GetAlbumPhotos.Data>> getAdd() {
        return mAdd;
    }

    public void hitGetAlbumApi(String type)
    {
        Call<GetAlbums> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getalbums("Bearer "+sharedPrefrencesMain.getToken(),type);

        verify.enqueue(new Callback<GetAlbums>() {
            @Override
            public void onResponse(Call<GetAlbums> call, Response<GetAlbums> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetAlbums> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });

    }

}