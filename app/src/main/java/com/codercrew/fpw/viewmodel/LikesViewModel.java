package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetLikes;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikesViewModel extends ViewModel {

    private MutableLiveData<ArrayList<GetLikes.LikesDetails>> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public LikesViewModel() {
        mText = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
    }

    public LiveData<ArrayList<GetLikes.LikesDetails>> getText() {
        return mText;
    }



    public void hitGetLikesApi(String pid)
    {
        Call<GetLikes> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getlikers("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetLikes>() {
            @Override
            public void onResponse(Call<GetLikes> call, Response<GetLikes> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetLikes> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}