package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.FamilyMemberWallAdapter;

import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetProfile;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.pagination.PaginationListener.PAGE_SIZE;
import static com.codercrew.fpw.pagination.PaginationListener.PAGE_START;

public class MemberProfileViewModel extends ViewModel {

    private MutableLiveData<GetProfile> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;
    public int currentPage = PAGE_START;
    public MemberProfileViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue(SetListValues());
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
    }


    public LiveData<GetProfile> getText() {
        return mText;
    }


    public void hitProfileApi(String id,String page)
    {
        Call<GetProfile> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getprofilebyid2("Bearer "+sharedPrefrencesMain.getToken(),id,page);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                if(response.body().getData().getWall().getPages().length()>0) {
                    PAGE_SIZE = response.body().getData().getWall().getPosts().size();
                    currentPage = Integer.parseInt(response.body().getData().getWall().getPages());
                }
                else currentPage = 0;
                mText.setValue(response.body());
            }

            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void hitLikeApi(String uid,String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.likepost("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if (response.isSuccessful()) {
//                    hitProfileApi(uid);
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitCommentApi(String uid,String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.addComment("Bearer "+sharedPrefrencesMain.getToken(), FamilyMemberWallAdapter.comment,postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                FamilyMemberWallAdapter.comment="";
                if (response.isSuccessful()) {
//                    hitProfileApi(uid);
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                FamilyMemberWallAdapter.comment="";
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitDeletePostApi(String uid,String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.deletepost("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                FamilyMemberWallAdapter.comment="";
                if (response.isSuccessful()) {
                    hitProfileApi(uid,"0");
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                FamilyMemberWallAdapter.comment="";
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}