package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetAlbumPhotos;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumsGalleryViewModel extends ViewModel {

    private MutableLiveData<ArrayList<GetAlbumPhotos.Data>> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public AlbumsGalleryViewModel() {
        mText = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());

    }

    public LiveData<ArrayList<GetAlbumPhotos.Data>> getText() {
        return mText;
    }



    public void hitGetAlbumApi(String pid,String type)
    {
        Call<GetAlbumPhotos> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getalbumGallery("Bearer "+sharedPrefrencesMain.getToken(),pid,type);

        verify.enqueue(new Callback<GetAlbumPhotos>() {
            @Override
            public void onResponse(Call<GetAlbumPhotos> call, Response<GetAlbumPhotos> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetAlbumPhotos> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void hitDelAlbumApi(String vpid,String type)
    {
        Call<GetAlbumPhotos> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.deletepv("Bearer "+sharedPrefrencesMain.getToken(),vpid,type);

        verify.enqueue(new Callback<GetAlbumPhotos>() {
            @Override
            public void onResponse(Call<GetAlbumPhotos> call, Response<GetAlbumPhotos> response) {
                if (response.isSuccessful()) {
//                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetAlbumPhotos> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}