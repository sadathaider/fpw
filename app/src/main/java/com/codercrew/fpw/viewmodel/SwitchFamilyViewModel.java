package com.codercrew.fpw.viewmodel;

import android.content.Intent;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.FamilySettingActivity;
import com.codercrew.fpw.SwitchFamilyActivity;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.model.FamilyModel;
import com.codercrew.fpw.model.GetFamilies;
import com.codercrew.fpw.model.SignUp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SwitchFamilyViewModel extends ViewModel {
    SharedPrefrencesMain sharedPrefrencesMain;

    private MutableLiveData<List<FamilyModel>> mText;

    public SwitchFamilyViewModel() {
        mText = new MutableLiveData<>();

        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
        hitFamilyApi();
    }

    public LiveData<List<FamilyModel>> getText() {
        return mText;
    }


    public void hitFamilyApi()
    {
        Call<GetFamilies> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getfamilies("Bearer "+sharedPrefrencesMain.getToken(),"");

        verify.enqueue(new Callback<GetFamilies>() {
            @Override
            public void onResponse(Call<GetFamilies> call, Response<GetFamilies> response) {
                if (response.isSuccessful()) {
                    if(response.body().getData()!=null)
                        mText.setValue(response.body().getData());
//                    return model;
                }
            }
            @Override
            public void onFailure(Call<GetFamilies> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitChangeFamilyApi(String fCode)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.changefamily("Bearer "+sharedPrefrencesMain.getToken(),fCode);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AppContext.getAppContext(), MainActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK); // In case we are called with non-Activity context.
                    AppContext.getAppContext().startActivity(intent);

                    if(SwitchFamilyActivity.switchFamilyActivity!=null)
                        SwitchFamilyActivity.switchFamilyActivity.finish();
                    if(FamilySettingActivity.familySettingActivity!=null)
                        FamilySettingActivity.familySettingActivity.finish();
                    if(MainActivity.mainActivity!=null)
                        MainActivity.mainActivity.finish();
//                    if(response.body().getData()!=null)
//                        hitFamilyApi();
                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitUnJoinFamilyApi(String fCode)
    {
        if(fCode.equals(sharedPrefrencesMain.getFmCode())) {
            Toast.makeText(AppContext.getAppContext(), "Can't remove your current family!", Toast.LENGTH_LONG).show();
            return;
        }
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.removefamily("Bearer "+sharedPrefrencesMain.getToken(),fCode);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
//                    if(response.body().getData()!=null)
                        hitFamilyApi();
                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}