package com.codercrew.fpw;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.SuggestedUsersChooseAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AddGroupMemberActivity extends AppCompatActivity {
    public static AddGroupMemberActivity groupMessagesActivity;
    private ProfileViewModel profileViewModel;
    private SuggestedUsersChooseAdapter familyMembersAdapter;
    ProgressDialog pd;
    private List<User> user = new ArrayList<>();
    private List<String> ids = new ArrayList<>();
    private com.codercrew.fpw.chat.model.User selectedUser;
    private SharedPrefrencesMain sharedPrefrencesMain;
    RecyclerView recyclerView;
    TextView textViewNext,textViewTitle;
    EditText editTextSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_messages);

        groupMessagesActivity = this;
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        recyclerView = findViewById(R.id.recyclerView);
        textViewNext = findViewById(R.id.tvNext);
        textViewTitle = findViewById(R.id.tvTitle);
        editTextSearch = findViewById(R.id.etTo);

        textViewNext.setText("Save");
        textViewTitle.setText("Add/Remove Member");
        selectedUser = (com.codercrew.fpw.chat.model.User) getIntent().getSerializableExtra("user");
        if(selectedUser.memberIds!=null && selectedUser.memberIds.length()>0){
            String[] split = selectedUser.memberIds.split(",");
            for(int i=0;i<split.length;i++){
                ids.add(split[i]);
            }
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });

        textViewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(familyMembersAdapter==null && familyMembersAdapter.GetSelectedUsers().size()==0){
                    Toast.makeText(AddGroupMemberActivity.this, "Select users to continue", Toast.LENGTH_SHORT).show();
                    return;
                }
                updateMember();

//                Intent intent = new Intent(AddGroupMemberActivity.this, CreateGroupActivity.class);
//                intent.putExtra("user", familyMembersAdapter.GetSelectedUsers());
//                startActivity(intent);
            }
        });
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                familyMembersAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        recyclerView.setOnClickListener(this);
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        pd.show();
        profileViewModel.hitProfileApi();

        profileViewModel.getText().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null && familyMembersModel.getData()!=null) {
                    pd.dismiss();
                    if(familyMembersModel.getData().getMembers()!=null && familyMembersModel.getData().getMembers().size()>0) {
                        user = familyMembersModel.getData().getMembers();
                        for(int i =0;i<user.size();i++){
                            for(String mem : ids){
                                if(user.get(i).getId().equals(mem))
                                    user.get(i).setSelected(true);
                            }
                        }

                        familyMembersAdapter = new SuggestedUsersChooseAdapter(getApplicationContext(), user, new OnItemClick() {
                            @Override
                            public void onClick(View view, int pos) {
                                if(view.getId()==R.id.imgMenu){
                                    if(ids.contains(familyMembersAdapter.GetSelectedUser(pos).getId())){
                                        showConfirmationDialog(pos,user.get(pos).getId());
                                    }else
                                        familyMembersAdapter.SelectUser(pos);
                                }
                            }
                        });
                        recyclerView.setAdapter(familyMembersAdapter);
                    }
                }
            }
        });
    }

    private void showConfirmationDialog(int pos,String pid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(AddGroupMemberActivity.this);
        builder1.setTitle("Remove member");
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FirebaseDatabase.getInstance().getReference().child("Friends").child(pid).child(selectedUser.name).removeValue();
                        ids.remove(pid);
                        familyMembersAdapter.SelectUser(pos);
                        Toast.makeText(AddGroupMemberActivity.this, "Removed successfully.", Toast.LENGTH_SHORT).show();
                        updateMember();

                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    private void updateMember(){
        com.codercrew.fpw.chat.model.User refuser = new com.codercrew.fpw.chat.model.User();
        refuser.id = sharedPrefrencesMain.getId();
        refuser.avata = sharedPrefrencesMain.getProfileUrl();
        refuser.email = sharedPrefrencesMain.getEmail();
        refuser.name = selectedUser.name;
        refuser.memberIds = "";
        for(int i=0;i<familyMembersAdapter.GetSelectedUsers().size();i++) {
            if(i==0)
                refuser.memberIds = familyMembersAdapter.GetSelectedUsers().get(i).getId();
            else refuser.memberIds = refuser.memberIds + "," + familyMembersAdapter.GetSelectedUsers().get(i).getId();
        }
        refuser.type = "0";

        FirebaseDatabase.getInstance().getReference().child("Friends").child(sharedPrefrencesMain.getId()).child(selectedUser.name).setValue(refuser);
        for(User user: familyMembersAdapter.GetSelectedUsers()){
            FirebaseDatabase.getInstance().getReference().child("Friends").child(user.getId()).child(selectedUser.name).setValue(refuser);
        }
        Toast.makeText(AddGroupMemberActivity.this, "Members updated successfully.", Toast.LENGTH_SHORT).show();
        finish();

    }
}