package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.codercrew.fpw.adapter.PendingMembersAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.viewmodel.PendingMembersViewModel;

import java.util.List;

public class PendingMembersActivity extends AppCompatActivity implements OnItemClick, View.OnClickListener{

    private PendingMembersAdapter pendingMembersAdapter;
    private PendingMembersViewModel pendingMembersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_members);

        pendingMembersViewModel =
                new ViewModelProvider(this).get(PendingMembersViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvPendingMember);
        final ImageView imageView = findViewById(R.id.imgBack);
        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(PendingMembersActivity.this,RecyclerView.VERTICAL,false));
        pendingMembersViewModel.getText().observe(this, new Observer<List<FamilyMembersModel>>() {
            @Override
            public void onChanged(@Nullable List<FamilyMembersModel> familyMembersModel) {
                pendingMembersAdapter = new PendingMembersAdapter(PendingMembersActivity.this,familyMembersModel, PendingMembersActivity.this);
                recyclerView.setAdapter(pendingMembersAdapter);
            }
        });


    }

    @Override
    public void onClick(View view, int pos) {

    }
    @Override
    public void onClick(View v) {
        finish();
    }
}