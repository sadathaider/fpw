package com.codercrew.fpw;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.SignUp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.vikktorn.picker.City;
import com.vikktorn.picker.State;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.EditProfileActivity.cityObject;
import static com.codercrew.fpw.EditProfileActivity.stateObject;

public class AppContext extends Application {

    private static Context appContext;
    static SharedPrefrencesMain sharedPrefrencesMain;
    public static Context getAppContext(){
        return appContext;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        sharedPrefrencesMain = new SharedPrefrencesMain(appContext);
        // get State from assets JSON
        if(sharedPrefrencesMain.getisLogin()){
            hitUpdateTokenApi(sharedPrefrencesMain.getToken(),FirebaseInstanceId.getInstance().getToken());
            utils.GetUnreadMessagesCount(sharedPrefrencesMain.getId());
        }
        try {
            getStateJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // get City from assets JSON
        try {
            getCityJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // GET STATE FROM ASSETS JSON
    public void getStateJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("states.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("states");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            State stateData = new State();
            if(Integer.parseInt(cit.getString("country_id"))==231) {
                stateData.setStateId(Integer.parseInt(cit.getString("id")));
                stateData.setStateName(cit.getString("name"));
                stateData.setCountryId(Integer.parseInt(cit.getString("country_id")));
                stateObject.add(stateData);
            }
        }
    }
    // GET CITY FROM ASSETS JSON
    public void getCityJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("cities.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("cities");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            City cityData = new City();

            cityData.setCityId(Integer.parseInt(cit.getString("id")));
            cityData.setCityName(cit.getString("name"));
            cityData.setStateId(Integer.parseInt(cit.getString("state_id")));
            cityObject.add(cityData);
        }

        ExecutorService service = Executors.newFixedThreadPool(4);
        service.submit(new Runnable() {
            public void run() {
                for( int i = 0; i < stateObject.size(); i++ ){
                    State state = stateObject.get(i);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if(!cityObject.stream().filter(o -> o.getStateId()==state.getStateId()).findFirst().isPresent()){
                            stateObject.remove(state);
                            i--;
                        }
                    }
                }
            }
        });

    }

    public static void hitUpdateTokenApi(String authToken,String token)
    {

        Call<GetPost> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.fcmTokem("Bearer "+authToken,token);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if(response.code()==401 ){
                    hitgetNewTokenApi();
                }
//                response.body().toString();
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {

            }
        });
    }

    private static void hitgetNewTokenApi()
    {

        Call<SignUp> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getnewtoken(sharedPrefrencesMain.getEmail());

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if(response.isSuccessful() && response.body().getStatus()){
//                    sharedPrefrencesMain.setIsLogin(true);
                    sharedPrefrencesMain.setToken(response.body().getData().getToken());

                }
//                response.body().toString();
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }

}
