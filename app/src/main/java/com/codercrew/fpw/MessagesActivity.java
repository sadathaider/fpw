package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.adapter.SuggestedUsersAdapter;
import com.codercrew.fpw.chat.ChatActivity;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.profile.ProfileFragment;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.home.ui.profile.adapter.FamilyMembersAdapter;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesActivity extends AppCompatActivity {
    public static MessagesActivity messagesActivity;
    private ProfileViewModel profileViewModel;
    private SuggestedUsersAdapter familyMembersAdapter;
    ProgressDialog pd;
    private List<User> user = new ArrayList<>();
    private SharedPrefrencesMain sharedPrefrencesMain;
    EditText editTextCreateGroup,editTextSearch;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        messagesActivity = this;
        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        recyclerView = findViewById(R.id.recyclerView);
        editTextCreateGroup = findViewById(R.id.etCreateGrp);
        editTextSearch = findViewById(R.id.etTo);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                familyMembersAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessagesActivity.this, GroupMessagesActivity.class);
                startActivity(intent);
            }
        });
//        recyclerView.setOnClickListener(this);
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        pd.show();
        profileViewModel.hitProfileApi();

        profileViewModel.getText().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null && familyMembersModel.getData()!=null) {
                    pd.dismiss();
                    if(familyMembersModel.getData().getMembers()!=null && familyMembersModel.getData().getMembers().size()>0) {
                        user = familyMembersModel.getData().getMembers();
                        familyMembersAdapter = new SuggestedUsersAdapter(getApplicationContext(), familyMembersModel.getData().getMembers(), new OnItemClick() {
                            @Override
                            public void onClick(View view, int pos) {
                                hitCanMessageApi(familyMembersAdapter.getSelectedUser(pos).getId(),pos);

                            }
                        });
                        recyclerView.setAdapter(familyMembersAdapter);
                    }
                }
            }
        });
    }

    public void hitCanMessageApi(String postId,int pos)
    {
        Call<GetProfile> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.canmessage("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
//                    Toast.makeText(getApplicationContext(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    if(response.body().getData().isCanmessage()){
                    user.get(pos).setType("1");
                    Intent intent = new Intent(MessagesActivity.this, ChatActivity.class);
                    intent.putExtra("user", familyMembersAdapter.getSelectedUser(pos));
                    intent.putExtra("canmessage",response.body().getData().isCanmessage());
                    intent.putExtra("message",response.body().getMessage());
                    startActivity(intent);
//                    }
                }
            }
            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}