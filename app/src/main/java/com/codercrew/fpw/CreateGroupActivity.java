package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.SuggestedUsersAdapter;
import com.codercrew.fpw.chat.ChatActivity;
import com.codercrew.fpw.chat.model.Message;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class CreateGroupActivity extends AppCompatActivity {
    private ProfileViewModel profileViewModel;
    private SuggestedUsersAdapter familyMembersAdapter;
    ProgressDialog pd;
    private ArrayList<User> user = new ArrayList<>();
    private SharedPrefrencesMain sharedPrefrencesMain;
    EditText editTextCreateGroup;
    RecyclerView recyclerView;
    TextView textViewSuggested,textViewCreate;
    EditText editTextName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        recyclerView = findViewById(R.id.recyclerView);
        editTextCreateGroup = findViewById(R.id.etCreateGrp);
        textViewSuggested = findViewById(R.id.tvSuggest);
        textViewCreate = findViewById(R.id.tvNext);
        editTextName = findViewById(R.id.etTo);

        user = (ArrayList<User>) getIntent().getSerializableExtra("user");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });

        textViewSuggested.setText(user.size()+" MEMBERS");

        textViewCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextName.getText().toString().isEmpty()){
                    editTextName.setError("Group name required!");
                    editTextName.requestFocus();
                    return;
                }

                com.codercrew.fpw.chat.model.User refuser = new com.codercrew.fpw.chat.model.User();
                refuser.id = sharedPrefrencesMain.getId();
                refuser.avata = sharedPrefrencesMain.getProfileUrl();
                refuser.email = sharedPrefrencesMain.getEmail();
                refuser.name = editTextName.getText().toString();
                for(int i=0;i<user.size();i++) {
                    if(i==0)
                        refuser.memberIds = user.get(i).getId();
                    else refuser.memberIds = refuser.memberIds + "," + user.get(i).getId();
                }
                refuser.type = "0";

                FirebaseDatabase.getInstance().getReference().child("Friends").child(sharedPrefrencesMain.getId()).child(editTextName.getText().toString()).setValue(refuser);
                for(User user: user){
                    FirebaseDatabase.getInstance().getReference().child("Friends").child(user.getId()).child(editTextName.getText().toString()).setValue(refuser);
                }

                if(MessagesActivity.messagesActivity!=null)
                    MessagesActivity.messagesActivity.finish();
                if(GroupMessagesActivity.groupMessagesActivity!=null)
                    GroupMessagesActivity.groupMessagesActivity.finish();
                Toast.makeText(CreateGroupActivity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        familyMembersAdapter = new SuggestedUsersAdapter(getApplicationContext(), user, new OnItemClick() {
            @Override
            public void onClick(View view, int pos) {
//                                Intent intent = new Intent(CreateGroupActivity.this, ChatActivity.class);
//                                intent.putExtra("user", user.get(pos));
//                                startActivity(intent);
            }
        });
        recyclerView.setAdapter(familyMembersAdapter);

    }
}