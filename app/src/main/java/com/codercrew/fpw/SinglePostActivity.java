package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.FamilyMemberWallAdapter;
import com.codercrew.fpw.adapter.SinglePostAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetLikes;
import com.codercrew.fpw.model.GetNotifications;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetPostPagination;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SinglePostActivity extends AppCompatActivity implements View.OnClickListener, OnItemClick{
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<GetPost.PostDetails> getProfile=new ArrayList<>();
    SinglePostAdapter singlePostAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();

        final RecyclerView recyclerView = findViewById(R.id.rvPost);
        final ImageView imageView = findViewById(R.id.imgBack);
        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(SinglePostActivity.this,RecyclerView.VERTICAL,false));
        singlePostAdapter = new SinglePostAdapter(getApplicationContext(), new ArrayList<>(),this);
        recyclerView.setAdapter(singlePostAdapter);
        hitPostApi(getIntent().getStringExtra("postid"));

    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }

    public void hitPostApi(String pid)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getpost("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    getProfile = response.body().getData();
                    singlePostAdapter.addItems(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        hitDeleteNotify(getIntent().getStringExtra("id"));
    }

    public void hitDeleteNotify(String pid)
    {
        Call<GetNotifications> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.delnotification("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetNotifications>() {
            @Override
            public void onResponse(Call<GetNotifications> call, Response<GetNotifications> response) {
                finish();
            }
            @Override
            public void onFailure(Call<GetNotifications> call, Throwable t) {
              finish();
            }
        });
    }

    @Override
    public void onClick(View view, int pos) {
        if(view.getId() == R.id.tvLikes){
            Intent intent = new Intent(getApplicationContext(), LikesActivity.class);
            intent.putExtra("pid",getProfile.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.tvComments){
            Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);
            intent.putExtra("pid",getProfile.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.imgLike){
//            pd.show();
            if(getProfile.get(pos).getIsliked().equals("0")) {
                getProfile.get(pos).setIsliked("1");
                getProfile.get(pos).setLikecount((Integer.parseInt(getProfile.get(pos).getLikecount()) + 1) + "");
                singlePostAdapter.notifyDataSetChanged();
                hitLikeApi( getProfile.get(pos).getId());
            }else {
                getProfile.get(pos).setIsliked("0");
                getProfile.get(pos).setLikecount((Integer.parseInt(getProfile.get(pos).getLikecount()) - 1) + "");
                singlePostAdapter.notifyDataSetChanged();
                hitLikeApi( getProfile.get(pos).getId());
            }
        }else if(view.getId()==R.id.imgSend){
//            pd.show();
            getProfile.get(pos).setCommentcount((Integer.parseInt(getProfile.get(pos).getCommentcount())+1)+"");
            singlePostAdapter.notifyDataSetChanged();
            hitCommentApi(getProfile.get(pos).getId());
        }
        else if(view.getId()==R.id.imgMenu){
//            PopupMenu popup = new PopupMenu(SinglePostActivity.this, view);
//            //Inflating the Popup using xml file
//            popup.getMenuInflater()
//                    .inflate(R.menu.popup_menu, popup.getMenu());
//
//            //registering popup with OnMenuItemClickListener
//            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                public boolean onMenuItemClick(MenuItem item) {
//                    if(item.getItemId()==R.id.edit){
//                        Intent intent = new Intent(getApplicationContext(), EditPostActivity.class);
//                        intent.putExtra("post",getProfile.get(pos));
//                        startActivity(intent);
//
//                    }else
////                        showConfirmationDialog(getProfile.get(pos).getId());
//                    return true;
//                }
//            });
//
//            popup.show(); //showing popup menu
        }
        else if(view.getId()==R.id.imgWall|| view.getId()==R.id.imgPlay){
            Intent intent = new Intent(getApplicationContext(), ViewPostActivity.class);
            intent.putExtra("post",getProfile.get(pos));
            intent.putExtra("from","notification");
            startActivity(intent);
        }
    }

    public void hitLikeApi(String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.likepost("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if (response.isSuccessful()) {
//                    hitProfileApi(uid);
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitCommentApi(String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.addComment("Bearer "+sharedPrefrencesMain.getToken(), FamilyMemberWallAdapter.comment,postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                FamilyMemberWallAdapter.comment="";
                if (response.isSuccessful()) {
//                    hitProfileApi(uid);
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                FamilyMemberWallAdapter.comment="";
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

}