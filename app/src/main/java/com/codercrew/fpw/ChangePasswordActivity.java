package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.model.SignUp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivCpassword,ivNpassword,ivRNpassword;
    private EditText etCpassword,etNpassword,etRNpassword;
    private boolean cPswd=false,nPswd=false,rnPswd=false;
    private Button buttonUpdate;
    private SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        final ImageView imageView = findViewById(R.id.imgBack);
        ivCpassword = findViewById(R.id.ivCode);
        ivNpassword = findViewById(R.id.ivNpassword);
        ivRNpassword = findViewById(R.id.ivRNpassword);

        etCpassword = findViewById(R.id.etCpassword);
        etNpassword = findViewById(R.id.etNpassword);
        etRNpassword = findViewById(R.id.etRNpassword);

        buttonUpdate = findViewById(R.id.btnUpdate);
        pd =new ProgressDialog(ChangePasswordActivity.this);
        pd.setMessage(getString(R.string.loading));

        imageView.setOnClickListener(this);

        ivCpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCpassword.requestFocus();
                if(cPswd){
                    etCpassword.setTransformationMethod(new PasswordTransformationMethod());
                    etCpassword.setSelection(etCpassword.getText().length());
                    cPswd = false;
                }else {
                    etCpassword.setTransformationMethod(null);
                    etCpassword.setSelection(etCpassword.getText().length());
                    cPswd = true;
                }
            }
        });

        ivNpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etNpassword.requestFocus();
                if(nPswd){
                    etNpassword.setTransformationMethod(new PasswordTransformationMethod());
                    etNpassword.setSelection(etNpassword.getText().length());
                    nPswd = false;
                }else {
                    etNpassword.setTransformationMethod(null);
                    etNpassword.setSelection(etNpassword.getText().length());
                    nPswd = true;
                }
            }
        });

        ivRNpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etRNpassword.requestFocus();
                if(rnPswd){
                    etRNpassword.setTransformationMethod(new PasswordTransformationMethod());
                    etRNpassword.setSelection(etRNpassword.getText().length());
                    rnPswd = false;
                }else {
                    etRNpassword.setTransformationMethod(null);
                    etRNpassword.setSelection(etRNpassword.getText().length());
                    rnPswd = true;
                }
            }
        });



        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidations();
            }
        });

        etCpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    etCpassword.setError("Space is not allowed");
                    etCpassword.setText(s.toString().trim());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etNpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    etNpassword.setError("Space is not allowed");
                    etNpassword.setText(s.toString().trim());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etRNpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    etRNpassword.setError("Space is not allowed");
                    etRNpassword.setText(s.toString().trim());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    @Override
    public void onClick(View v) {
        finish();
    }

    private void SetValidations() {
        if(!utils.CheckInternet(getApplicationContext())){
            Toast.makeText(this, "Check your internet connection!", Toast.LENGTH_SHORT).show();
            return;
        }

        String passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$";

        //Password Validations
        if(etCpassword.getText().toString().isEmpty()) {
            etCpassword.setError("enter password");
            etCpassword.requestFocus();
            return;
        }
        if(etNpassword.getText().toString().isEmpty()) {
            etNpassword.setError("enter new password");
            etNpassword.requestFocus();
            return;
        }
        if(!etNpassword.getText().toString().matches(passwordPattern)) {
            etNpassword.setError("Invalid password");
            etNpassword.requestFocus();
            Toast.makeText(getApplicationContext(), "Be at least 8 characters long\n" +
                    "Include at least: 1 upper case letter, 1 lower case letter, 1 number and 1 symbol", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!etRNpassword.getText().toString().equals(etNpassword.getText().toString())) {
            etRNpassword.setError("Password not match");
            etRNpassword.requestFocus();
            return;
        }

        hitPasswordApi();
    }

    private void hitPasswordApi() {
        pd.show();

        Call<SignUp> signup;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        signup = apiInterface.updatepassword("Bearer "+sharedPrefrencesMain.getToken(),etCpassword.getText().toString().trim(),
                etNpassword.getText().toString().trim()
        );


        signup.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    SignUp model = response.body();
                    if(model!=null)
                        Toast.makeText(ChangePasswordActivity.this, ""+model.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                pd.dismiss();

                Toast.makeText(ChangePasswordActivity.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}