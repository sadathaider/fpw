package com.codercrew.fpw;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.AlbumsAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.media.MediaFragment;
import com.codercrew.fpw.model.GetAlbums;
import com.codercrew.fpw.viewmodel.AlbumsViewModel;

import java.util.ArrayList;

public class AlbumsActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {

    private AlbumsAdapter albumAdapter;
    protected AlbumsViewModel AlbumsViewModel;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<GetAlbums.Data> getAlbums;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        type = getIntent().getStringExtra("type");

        AlbumsViewModel = new ViewModelProvider(this).get(AlbumsViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        final ImageView imageViewSave = findViewById(R.id.imgAdd);
        final TextView textViewTitle = findViewById(R.id.tvTitle);

        imageView.setOnClickListener(this);
        imageViewSave.setOnClickListener(this);

        if(type.equals("1")){
            textViewTitle.setText("Photo Album");
        }else if(type.equals("2")){
            textViewTitle.setText("Video Album");
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(AlbumsActivity.this,RecyclerView.VERTICAL,false));
        AlbumsViewModel.getText().observe(this, new Observer<ArrayList<GetAlbums.Data>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetAlbums.Data> familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null) {
                    getAlbums = familyMembersModel;
                    albumAdapter = new AlbumsAdapter(AlbumsActivity.this, familyMembersModel, AlbumsActivity.this);
                    recyclerView.setAdapter(albumAdapter);
                }
            }
        });


    }

    @Override
    public void onClick(View view, int pos) {
        if(view.getId()==R.id.constraintLayout){
            Intent intent = new Intent(getApplicationContext(),AlbumGalleryActivity.class);
            intent.putExtra("type",getIntent().getStringExtra("type"));
            intent.putExtra("id",getAlbums.get(pos).getId());
            intent.putExtra("name",getAlbums.get(pos).getAlbumname());
            startActivity(intent);
        }else if(view.getId()==R.id.imgDelete){
            showConfirmationDialog(pos,getAlbums.get(pos).getId());
        }

    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgAdd){
            Intent intent = new Intent(getApplicationContext(),AddAlbumActivity.class);
            intent.putExtra("type",getIntent().getStringExtra("type"));
            intent.putExtra("id","");
            startActivity(intent);
        }else {
            finish();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();
        AlbumsViewModel =
                new ViewModelProvider(this).get(AlbumsViewModel.class);
        if( MediaFragment.ISMEDIAUPDATED) {
            MediaFragment.ISMEDIAUPDATED =false;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AlbumsViewModel.hitGetAlbumApi(getIntent().getStringExtra("type"));
                }
            }, 3000);
        }else
            AlbumsViewModel.hitGetAlbumApi(getIntent().getStringExtra("type"));

    }

    private void showConfirmationDialog(int pos,String pid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(AlbumsActivity.this);

        builder1.setTitle("Delete Album");
        builder1.setMessage("Are you sure?");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        arrayList.remove(pos);
                        albumAdapter.clearItem(pos);
                        AlbumsViewModel.hitDelAlbumApi(pid,getIntent().getStringExtra("type"));
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}