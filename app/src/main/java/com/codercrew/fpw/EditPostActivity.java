package com.codercrew.fpw;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.ProgressRequestBody;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.ui.newsfeed.NewsfeedViewModel;
import com.codercrew.fpw.home.ui.walls.WallsFragment;
import com.codercrew.fpw.model.GetPost;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.codercrew.fpw.model.GetPostPagination;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class EditPostActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks{
    SharedPrefrencesMain sharedPrefrencesMain;
    private NewsfeedViewModel newsfeedViewModel;
    ProgressDialog pd;
    String url,type;
    EditText editTextTitle;
    private static final int PICK_IMAGE = 100,CAMERA=2,PICK_VIDEO=200,CAMERAVIDEO=300;
    private static final String IMAGE_DIRECTORY = "/encoded_image";
    String desc = "";
    boolean isFileSelect = false;
    ImageView imageViewSelect;
    ImageView imageViewClose;
    String filePAth="";
    EditText editTextCap;
    Uri cameraUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        sharedPrefrencesMain = new SharedPrefrencesMain(getApplicationContext());
        pd =new ProgressDialog(this);
        pd.setIndeterminate(false);
        pd.setCancelable(false);
//        pd.show();
        final ImageView imageViewProfile = findViewById(R.id.imgProfile);
        imageViewSelect = findViewById(R.id.ivSelectedPic);
        final  ImageView imageViewPost = findViewById(R.id.ivPost);
        final  ImageView ivImagePost = findViewById(R.id.ivImagePost);
        final  ImageView ivVideoPost = findViewById(R.id.ivVideoPost);
        final TextView textViewName = findViewById(R.id.tvName);
        final EditText textViewTitle = findViewById(R.id.tvTitle);
        editTextCap = findViewById(R.id.etCaption);
        imageViewClose = findViewById(R.id.imgClose);

        //receive
        GetPostPagination.Data.PostDetails model =  (GetPostPagination.Data.PostDetails) getIntent().getSerializableExtra("post");
        if(!model.getText().isEmpty())
            textViewTitle.setText(model.getText());

        if(model.getCaption()!=null && !model.getCaption().isEmpty())
            editTextCap.setText(model.getCaption());

        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewSelect.setImageBitmap(null);
                type = "0";
                imageViewClose.setVisibility(View.GONE);
            }
        });

        type = model.getType();
        if(model.getType().equals("0")){
//            imageViewSelect.setVisibility(View.GONE);
            imageViewPost.setVisibility(View.VISIBLE);
            imageViewClose.setVisibility(View.GONE);
        }else if(model.getType().equals("1")){
            Glide.with(this).load(model.getImage()).into(imageViewSelect);
            ivImagePost.setVisibility(View.VISIBLE);
        }else if(model.getType().equals("2")){
            ivVideoPost.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            Glide.with(this).setDefaultRequestOptions(requestOptions).load(model.getVideo()).into(imageViewSelect);
        }

        if(!sharedPrefrencesMain.getProfileUrl().isEmpty())
            Glide.with(this).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);
        textViewName.setText(sharedPrefrencesMain.getName());

        ivVideoPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!textViewTitle.getText().toString().isEmpty())
                    desc = textViewTitle.getText().toString();
                showVideoDialog();
//                openGalleryVideo();
            }
        });

        ivImagePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!textViewTitle.getText().toString().isEmpty())
                    desc = textViewTitle.getText().toString();
                openGallery();
            }
        });

        imageViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String caption = "";
                if(type.equals("0")) {
                    if (textViewTitle.getText().toString().isEmpty()) {
                        textViewTitle.setError("Enter something");
                        textViewTitle.requestFocus();
                        return;
                    }
                    desc = textViewTitle.getText().toString();
                    hitPostApi(desc, model.getId());
                }else {
                    if(!isFileSelect){
                        if(!textViewTitle.getText().toString().isEmpty())
                            desc = textViewTitle.getText().toString();
                        if(!editTextCap.getText().toString().isEmpty())
                            caption = editTextCap.getText().toString();
                        hitPostImageTextApi(desc,model.getId(),type,caption);
                    }else {
                        if(!textViewTitle.getText().toString().isEmpty())
                            desc = textViewTitle.getText().toString();
                        if(!editTextCap.getText().toString().isEmpty())
                            caption = editTextCap.getText().toString();
                        callApiUploadImage(filePAth,desc,type,model.getId(),caption);
                    }
                }
            }
        });
    }

    private void callApiUploadImage(String path,String text,String type,String id,String cap) {
        if (!path.isEmpty()){
            File file = new File(path);
            pd.show();
            MultipartBody.Part part;
            RequestBody requestBodyId = RequestBody.create(MediaType.parse("application/octet-stream"),id);
            RequestBody requestBodyText = RequestBody.create(MediaType.parse("application/octet-stream"),text);
            RequestBody requestBodyType = RequestBody.create(MediaType.parse("application/octet-stream"),type);
            RequestBody requestBodyCode = RequestBody.create(MediaType.parse("application/octet-stream"),sharedPrefrencesMain.getFmCode());
            RequestBody requestBodyCap = RequestBody.create(MediaType.parse("application/octet-stream"),cap);

            ProgressRequestBody fileBody;
            if(type.equals("1")) {
                fileBody = new ProgressRequestBody(file,"image", this);
                part = MultipartBody.Part.createFormData("image", file.getName(), fileBody);
            }
            else {
                fileBody = new ProgressRequestBody(file,"video", this);
                part = MultipartBody.Part.createFormData("video",file.getName(),fileBody);
            }
            Call<GetPost> apiResponseUploadImageCall = ApiClient.getClient().create(ApiInterface.class).editImagePost("Bearer "+sharedPrefrencesMain.getToken(),requestBodyId,requestBodyCode,requestBodyText,requestBodyType,requestBodyCap,part);
            apiResponseUploadImageCall.enqueue(new Callback<GetPost>() {
                @Override
                public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                    pd.dismiss();
                    if (response.isSuccessful()){
                        file.delete();
                        MediaScannerConnection.scanFile(EditPostActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            /*
                             *   (non-Javadoc)
                             * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                             */
                            public void onScanCompleted(String path, Uri uri) {
                                Log.e("ExternalStorage", "Scanned " + path + ":");
                                Log.e("ExternalStorage", "-> uri=" + uri);
                            }
                        });
                        if (response.body()!=null){
                            WallsFragment.ISUPDATED=true;
                            finish();
                            Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(AppContext.getAppContext(), ""+response.errorBody(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetPost> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(AppContext.getAppContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(AppContext.getAppContext(), "No such file Found", Toast.LENGTH_SHORT).show();
        }
    }

    public void hitPostApi(String text,String id)
    {
        pd.show();
        Call<GetPost> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.editTextPost("Bearer "+sharedPrefrencesMain.getToken(),id,sharedPrefrencesMain.getFmCode(),text,"0");

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    WallsFragment.ISUPDATED=true;
                   finish();
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitPostImageTextApi(String text,String id,String type,String cap)
    {
        pd.show();
        Call<GetPost> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.editTextImagePost("Bearer "+sharedPrefrencesMain.getToken(),id,sharedPrefrencesMain.getFmCode(),text,type,cap);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    WallsFragment.ISUPDATED=true;
                    finish();
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void showVideoDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(EditPostActivity.this);
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGalleryVideo();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    private void showPictureDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(EditPostActivity.this);
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }
    private void openGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("image/*");
                startActivityForResult(gallery, PICK_IMAGE);
            }
            else {
                ActivityCompat.requestPermissions( this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, PICK_IMAGE);
        }
    }


    private void openGalleryVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("video/*");
                startActivityForResult(gallery, PICK_VIDEO);
            }
            else {
                ActivityCompat.requestPermissions( this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 6);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("video/*");
            startActivityForResult(gallery, PICK_VIDEO);
        }
    }

    private void takePhotoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
                    cameraUri = FileProvider.getUriForFile(getApplicationContext(),"com.codercrew.fpw.provider",new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));
                }else
                    cameraUri = FileProvider.getUriForFile(getApplicationContext(),"com.codercrew.fpw.provider",new File(Environment.getExternalStorageDirectory(), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
//                    intent.setType("image/*");
                startActivityForResult(intent, CAMERA);
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, 4);
            }
        }else {

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.setType("image/*");
            startActivityForResult(intent, CAMERA);
        }
    }
    private void takeVideoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    intent.setType("video/*");
                startActivityForResult(intent, CAMERAVIDEO);
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, 7);
            }
        }else {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
//                intent.setType("video/*");
            startActivityForResult(intent, CAMERAVIDEO);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        isFileSelect = true;
        imageViewClose.setVisibility(View.VISIBLE);
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                type = "1";
                Uri contentURI = data.getData();
                UCrop.of(data.getData(), Uri.fromFile(new File(getCacheDir(), "IMG_" + System.currentTimeMillis())))
                        .start(EditPostActivity.this);
            }
        }else if (requestCode == PICK_VIDEO) {
            if (data != null) {
                type = "2";
                Uri contentURI = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                filePAth = picturePath;
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.MICRO_KIND);
                imageViewSelect.setImageBitmap(bitmap);
            }
        } else if (requestCode == CAMERA) {
            type = "1";
            UCrop.of(cameraUri, Uri.fromFile(new File(getCacheDir(), "IMG_" + System.currentTimeMillis())))
                    .start(EditPostActivity.this);
//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            imageViewSelect.setImageBitmap(thumbnail);
//            saveImage(thumbnail);

            Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }else if(requestCode == CAMERAVIDEO){
            if (data != null) {
                type = "2";
                Uri contentURI = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                filePAth = picturePath;
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.MICRO_KIND);
                imageViewSelect.setImageBitmap(bitmap);
            }
        }else if(requestCode==UCrop.REQUEST_CROP){
            Uri imgUri = UCrop.getOutput(data);
            if (imgUri != null) {
                String selectedImage = imgUri.getPath();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    imageViewSelect.setImageBitmap(bitmap);
                    String path = saveImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
                // load selectedImage into ImageView
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 4) {
            takePhotoFromCamera();
        }
        else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 5) {
            openGallery();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 6) {
            openGalleryVideo();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 7) {
            takeVideoFromCamera();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            wallpaperDirectory = new File(
                    getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + IMAGE_DIRECTORY);
        }else {
            wallpaperDirectory = new File(
                    Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        }
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            if (!f.getParentFile().exists())
                f.getParentFile().mkdirs();
            if (!f.exists())
                f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);

            fo.close();
            filePAth = f.getAbsolutePath();
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onProgressUpdate(int percentage) {
        pd.setMessage(percentage+"% Uploading...");
        pd.show();
    }

    @Override
    public void onError() {
        pd.dismiss();
    }

    @Override
    public void onFinish() {
    }
}