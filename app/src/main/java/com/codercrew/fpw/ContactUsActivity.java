package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetUsers;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText editTextMessage;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog  progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        final ImageView imageView = findViewById(R.id.imgBack);
        final Button buttonSubmit = findViewById(R.id.btnSubmit);
        editTextMessage = findViewById(R.id.etMessage);
        imageView.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        progressDialog = new ProgressDialog(ContactUsActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgBack) {
            finish();
        }else if(v.getId()==R.id.btnSubmit){
            if(editTextMessage.getText().toString().isEmpty()){
                editTextMessage.setError("Enter message");
                editTextMessage.requestFocus();
                return;
            }
            progressDialog.show();
            hitContactApi(editTextMessage.getText().toString());
        }
    }

    public void hitContactApi(String pid)
    {
        Call<GetUsers> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.contactus("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetUsers>() {
            @Override
            public void onResponse(Call<GetUsers> call, Response<GetUsers> response) {
                progressDialog.dismiss();
                Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                if (response.isSuccessful()) {
                    editTextMessage.setText("");
                    finish();
                }
            }
            @Override
            public void onFailure(Call<GetUsers> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}