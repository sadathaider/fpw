package com.codercrew.fpw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.model.SignUp;
import com.bumptech.glide.Glide;
import com.vikktorn.picker.City;
import com.vikktorn.picker.CityPicker;
import com.vikktorn.picker.Country;
import com.vikktorn.picker.CountryPicker;
import com.vikktorn.picker.OnCityPickerListener;
import com.vikktorn.picker.OnCountryPickerListener;
import com.vikktorn.picker.OnStatePickerListener;
import com.vikktorn.picker.State;
import com.vikktorn.picker.StatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class EditProfileActivity extends AppCompatActivity implements OnStatePickerListener, OnCountryPickerListener, OnCityPickerListener {

    private ConstraintLayout constraintLayout;
    private TextView textViewDone;
    private EditText editTextName,editTextEmail,editTextCity,editTextState,editTextPosCode,editTextCountry,editTextDob;

    private ImageView imageViewGallery,imageViewProfile;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;
    int genderSelected;
    private int mYear, mMonth, mDay;
    private static final int PICK_IMAGE = 100,CAMERA=2;
    private static final String IMAGE_DIRECTORY = "/encoded_image";
    private LinearLayout linearLayout;
    public int countryID, stateID,countryFlag;
    // Pickers
    private CountryPicker countryPicker;
    private StatePicker statePicker;
    private CityPicker cityPicker;
    // arrays of state object
    public static List<State> stateObject = new ArrayList<>();
    // arrays of city object
    public static List<City> cityObject = new ArrayList<>();
    RadioButton radioButton1,radioButton2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        pd = new ProgressDialog(EditProfileActivity.this);
        pd.setMessage(getString(R.string.loading));





        // initialize country picker
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();


        constraintLayout = findViewById(R.id.constraintLayout);
        imageViewGallery = findViewById(R.id.imgGallery);
        imageViewProfile = findViewById(R.id.imgProfile);
        editTextName = findViewById(R.id.etName);
        editTextEmail = findViewById(R.id.etEmail);
        editTextCity = findViewById(R.id.etCity);
        editTextState = findViewById(R.id.etState);
        editTextPosCode = findViewById(R.id.etPosCode);
        editTextCountry = findViewById(R.id.etCountry);
        editTextDob = findViewById(R.id.etDob);
        textViewDone = findViewById(R.id.tvDone);



        linearLayout = findViewById(R.id.rlImage);

        radioButton1 = findViewById(R.id.male);
        radioButton2 = findViewById(R.id.female);

        hitProfileApi();

        countryFlag = 2131165550;
        countryID = 231;
        editTextCountry.setText("United States");
        sharedPrefrencesMain.setCountryId(countryID+"");
        sharedPrefrencesMain.setCountryFlag(countryFlag);
        // initialize listeners
        setListener();
//        setCountryListener();
        setCityListener();






        if(sharedPrefrencesMain.getProfileUrl().length()>0)
            Glide.with(AppContext.getAppContext()).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);

        editTextDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogue();
            }
        });

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderSelected = 1;
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderSelected = 0;
            }
        });

        textViewDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidations();
            }
        });

        imageViewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        constraintLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        linearLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    hideKeyboard(v);
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void datePickerDialogue() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        editTextDob.setText(getDate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));
//                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    }

                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void SetValidations() {
        if(!utils.CheckInternet(getApplicationContext())){
            Toast.makeText(this, "Check your internet connection!", Toast.LENGTH_SHORT).show();
            return;
        }
        String firstNamePattern = "[a-zA-Z]+[ ]*";
        String lastNamePattern = "[a-zA-Z]+[ ]*+[a-zA-Z]*+[ ]*";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+[ ]*";
        String passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$";
        //First Name Validations
        if(editTextName.getText().toString().isEmpty()) {
            editTextName.setError("enter first name");
            editTextName.requestFocus();
            return;
        }
//        else if(!editTextName.getText().toString().matches(firstNamePattern)){
//            editTextName.setError("Invalid first name");
//            editTextName.requestFocus();
//            return;
//        }

        //Email Validations
        if(editTextEmail.getText().toString().isEmpty()) {
            editTextEmail.setError("enter email address");
            editTextEmail.requestFocus();
            return;
        }
        else if(!editTextEmail.getText().toString().matches(emailPattern)){
            editTextEmail.setError("Invalid email address");
            editTextEmail.requestFocus();
            return;
        }
        //City Validations
        if(editTextCity.getText().toString().isEmpty()) {
            editTextCity.setError("enter city");
            editTextCity.requestFocus();
            return;
        }

        //State Validations
        if(editTextState.getText().toString().isEmpty()) {
            editTextState.setError("enter state");
            editTextState.requestFocus();
            return;
        }

        //Zip Validations
        if(editTextPosCode.getText().toString().isEmpty()) {
            editTextPosCode.setError("enter postal code");
            editTextPosCode.requestFocus();
            return;
        }

        //Country Validations
        if(editTextCountry.getText().toString().isEmpty()) {
            editTextCountry.setError("enter country");
            editTextCountry.requestFocus();
            return;
        }

        //DOB Validations
        if(editTextDob.getText().toString().isEmpty()) {
            editTextDob.setError("enter date of birth");
            editTextDob.requestFocus();
            return;
        }

        hitUpdateProfileApi();
    }
    private void hitUpdateProfileApi() {
        pd.show();

        Call<SignUp> signup;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        signup = apiInterface.updateprofile("Bearer " + sharedPrefrencesMain.getToken(), editTextName.getText().toString(), editTextEmail.getText().toString().trim(),
                "", editTextState.getText().toString().trim(), editTextPosCode.getText().toString().trim(), editTextCountry.getText().toString().trim(), editTextDob.getText().toString().trim(),
                editTextCity.getText().toString().trim(), genderSelected
        );


        signup.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    sharedPrefrencesMain.setCountryId(countryID+"");
                    sharedPrefrencesMain.setCountryFlag(countryFlag);
                    sharedPrefrencesMain.setStateId(stateID+"");
                    SignUp model = response.body();
                    if (model != null)
                        Toast.makeText(EditProfileActivity.this, "" + model.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else
                    Toast.makeText(EditProfileActivity.this, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                pd.dismiss();

                Toast.makeText(EditProfileActivity.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

        private void showPictureDialog(){

            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
            pictureDialog.setTitle("Choose From");
            String[] pictureDialogItems = {
                    "Gallery",
                    "Camera"};
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    openGallery();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();

        }

        private void openGallery() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, PICK_IMAGE);
                }
                else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
                }
            }else {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("image/*");
                startActivityForResult(gallery, PICK_IMAGE);
            }
        }

        private void takePhotoFromCamera() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    intent.setType("image/*");
                    startActivityForResult(intent, CAMERA);
                }
                else {
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, 4);
                }
            }else {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.setType("image/*");
                startActivityForResult(intent, CAMERA);
            }
        }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    pd.show();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
//                    Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    imageViewProfile.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            imageViewProfile.setImageBitmap(thumbnail);
            pd.show();
            saveImage(thumbnail);
//            Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 4) {
            takePhotoFromCamera();
        }
        else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 5) {
            openGallery();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            wallpaperDirectory = new File(
                    getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + IMAGE_DIRECTORY);
        }else {
            wallpaperDirectory = new File(
                    Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        }
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            if (!f.getParentFile().exists())
                f.getParentFile().mkdirs();
            if (!f.exists())
                f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            callApiUploadImage(f.getAbsolutePath());
            return f.getAbsolutePath();
        } catch (IOException e1) {
            pd.dismiss();
            e1.printStackTrace();
        }
        return "";
    }

    private void callApiUploadImage(String path) {
        if (!path.isEmpty()){
            File file = new File(path);
//            pd.show();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/octet-stream"),file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("profilepic",file.getName(),requestBody);
            Call<SignUp> apiResponseUploadImageCall = ApiClient.getClient().create(ApiInterface.class).updateprofile("Bearer "+sharedPrefrencesMain.getToken(),part);
            apiResponseUploadImageCall.enqueue(new Callback<SignUp>() {
                @Override
                public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                    pd.dismiss();
                    if (response.isSuccessful()){
                        if (response.body()!=null){
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), ""+response.errorBody(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SignUp> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(getApplicationContext(), "No such file Found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public String getDate(String dte){
        String s = dte;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            Date d = sdf.parse(s);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
            s = sdf2.format(d);
            return s;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void onResume(){
        super.onResume();


//        }
    }

    // ON SELECTED COUNTRY ADD STATES TO PICKER
    @Override
    public void onSelectCountry(Country country) {
        // get country name and country ID
//        editTextCountry.setText(country.getName());
        editTextState.setText("");
        editTextCity.setFocusable(false);
        editTextCity.setText("");
        countryID = country.getCountryId();
        countryFlag = country.getFlag();

        statePicker.equalStateObject.clear();
        cityPicker.equalCityObject.clear();

        //set state name text view and state pick button invisible
//        pickStateButton.setVisibility(View.VISIBLE);
//        stateNameTextView.setVisibility(View.VISIBLE);
//        stateNameTextView.setText("Region");
//        cityName.setText("City");
//        // set text on main view
//        countryCode.setText("Country code: " + country.getCode());
//        countryPhoneCode.setText("Country dial code: " + country.getDialCode());
//        countryCurrency.setText("Country currency: " + country.getCurrency());
//        flagImage.setBackgroundResource(country.getFlag());


        // GET STATES OF SELECTED COUNTRY
        for(int i = 0; i < stateObject.size(); i++) {
            // init state picker
            statePicker = new StatePicker.Builder().with(this).listener(this).build();
            State stateData = new State();
            if (stateObject.get(i).getCountryId() == countryID) {

                stateData.setStateId(stateObject.get(i).getStateId());
                stateData.setStateName(stateObject.get(i).getStateName());
                stateData.setCountryId(stateObject.get(i).getCountryId());
                stateData.setFlag(country.getFlag());
                statePicker.equalStateObject.add(stateData);
            }
        }
    }
    // ON SELECTED STATE ADD CITY TO PICKER
    @Override
    public void onSelectState(State state) {

        cityPicker.equalCityObject.clear();

        editTextState.setText(state.getStateName());
        editTextCity.setFocusable(false);
        editTextCity.setText("");
        stateID = state.getStateId();



        if(cityObject.size()>0) {
            for (int i = 0; i < cityObject.size(); i++) {
                cityPicker = new CityPicker.Builder().with(this).listener(this).build();

                City cityData = new City();
                if (cityObject.get(i).getStateId() == stateID) {
                    cityData.setCityId(cityObject.get(i).getCityId());
                    cityData.setCityName(cityObject.get(i).getCityName());
                    cityData.setStateId(cityObject.get(i).getStateId());

                    cityPicker.equalCityObject.add(cityData);
                }
            }
        }
    }
    // ON SELECTED CITY
    @Override
    public void onSelectCity(City city) {
        editTextCity.setText(city.getCityName());
    }

    // SET STATE LISTENER
    private void setListener() {
        editTextState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // GET STATES OF SELECTED COUNTRY
                if (!sharedPrefrencesMain.getCountryId().isEmpty()) {
                    for (int i = 0; i < stateObject.size(); i++) {
                        // init state picker
                        statePicker = new StatePicker.Builder().with(EditProfileActivity.this).listener(EditProfileActivity.this).build();
                        State stateData = new State();
                        if (stateObject.get(i).getCountryId() == Integer.parseInt(sharedPrefrencesMain.getCountryId())) {

                            stateData.setStateId(stateObject.get(i).getStateId());
                            stateData.setStateName(stateObject.get(i).getStateName());
                            stateData.setCountryId(stateObject.get(i).getCountryId());
                            stateData.setFlag(sharedPrefrencesMain.getCountryFlag());
                            statePicker.equalStateObject.add(stateData);

                        }
                    }
                }
                if(statePicker!=null) {
                    statePicker.showDialog(getSupportFragmentManager());
                }

            }
        });
    }

    //SET CITY LISTENER
    private void setCityListener() {
        editTextCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(cityPicker!=null)
                        cityPicker.showDialog(getSupportFragmentManager());
                }catch (IllegalArgumentException illegalArgumentException){
                    illegalArgumentException.getLocalizedMessage();

//                    editTextCity.setFocusableInTouchMode(true);
//                    editTextCity.setFocusable(true);
//                    editTextCity.requestFocus();
                }

            }
        });
    }
    //SET COUNTRY LISTENER
    private void setCountryListener() {
        editTextCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countryPicker.showDialog(getSupportFragmentManager());
            }
        });
    }


    // GET STATE FROM ASSETS JSON
    public void getStateJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("states.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("states");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            State stateData = new State();

            stateData.setStateId(Integer.parseInt(cit.getString("id")));
            stateData.setStateName(cit.getString("name"));
            stateData.setCountryId(Integer.parseInt(cit.getString("country_id")));
            stateObject.add(stateData);
        }
    }
    // GET CITY FROM ASSETS JSON
    public void getCityJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("cities.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("cities");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            City cityData = new City();

            cityData.setCityId(Integer.parseInt(cit.getString("id")));
            cityData.setCityName(cit.getString("name"));
            cityData.setStateId(Integer.parseInt(cit.getString("state_id")));
            cityObject.add(cityData);
        }

            for( int i = 0; i < stateObject.size(); i++ ){
                State state = stateObject.get(i);
//                for (State state : stateObject){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if(!cityObject.stream().filter(o -> o.getStateId()==state.getStateId()).findFirst().isPresent()){
                        stateObject.remove(state);
                        i--;
                    }
                }
            }

//        Iterator<State> itr = stateObject.iterator();
//        while(itr.hasNext()){
//            State state = itr.next();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                if(!cityObject.stream().filter(o -> o.getStateId()==state.getStateId()).findFirst().isPresent()){
//                    itr.remove();
//                }
//            }
//        }

    }

    public void hitProfileApi()
    {
        pd.show();
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getProfile("Bearer "+sharedPrefrencesMain.getToken(),"");

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pd.dismiss();
                if (response.isSuccessful()) {

                    String str = response.body().getData().getUser().getName();
                    String nameCap = str.substring(0, 1).toUpperCase() + str.substring(1);

                    editTextName.setText(nameCap);
                    editTextEmail.setText(response.body().getData().getUser().getEmail());
                    editTextDob.setText(getDate(response.body().getData().getUser().getDob()));
                    editTextPosCode.setText(response.body().getData().getUser().getZip());
                    editTextCity.setText(response.body().getData().getUser().getCity());
                    editTextState.setText(response.body().getData().getUser().getState());
//                    editTextCountry.setText(response.body().getData().getUser().getCountry());

                    if (response.body().getData().getUser().getGender()==1) {
                        genderSelected = 1;
                        radioButton1.setChecked(true);
                        radioButton1.setSelected(true);
                    } else if (response.body().getData().getUser().getGender()==0) {
                        genderSelected = 0;
                        radioButton2.setChecked(true);
                        radioButton2.setSelected(true);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if(!editTextState.getText().toString().isEmpty()){
                            try {
                                State state = stateObject.stream().filter(o -> o.getStateName().equals(editTextState.getText().toString())).findFirst().get();
                                stateID = state.getStateId();
                            }catch (NoSuchElementException n){
                                n.getMessage();
                            }

                        }
                    }

//        if (!sharedPrefrencesMain.getStateId().isEmpty()) {
                    if (cityObject.size() > 0) {
//                cityPicker.equalCityObject.clear();
                        for (int i = 0; i < cityObject.size(); i++) {
                            cityPicker = new CityPicker.Builder().with(EditProfileActivity.this).listener(EditProfileActivity.this).build();
                            City cityData = new City();
                            if (cityObject.get(i).getStateId() == stateID) {

                                cityData.setCityId(cityObject.get(i).getCityId());
                                cityData.setCityName(cityObject.get(i).getCityName());
                                cityData.setStateId(cityObject.get(i).getStateId());
                                cityPicker.equalCityObject.add(cityData);

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
               pd.dismiss();
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}