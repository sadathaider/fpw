package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.CountingFileRequestBody;
import com.codercrew.fpw.Utils.ProgressRequestBody;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.ui.newsfeed.NewsfeedViewModel;
import com.codercrew.fpw.home.ui.walls.WallsFragment;
import com.codercrew.fpw.model.GetPost;
import com.bumptech.glide.Glide;

import java.io.File;
import java.net.URISyntaxException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class PostActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks{
    SharedPrefrencesMain sharedPrefrencesMain;
    private NewsfeedViewModel newsfeedViewModel;
    ProgressDialog pd;
    String url,type,desc;
    EditText editTextTitle;
    Long futureTimeInMillis=0L;
    boolean isFutureMessage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        sharedPrefrencesMain = new SharedPrefrencesMain(getApplicationContext());
        pd =new ProgressDialog(this);
        pd.setIndeterminate(false);
        pd.setCancelable(false);

//        pd.show();
        final ImageView imageViewProfile = findViewById(R.id.imgProfile);
        final ImageView imageViewSelect = findViewById(R.id.ivSelectedPic);
        final  ImageView imageViewPost = findViewById(R.id.ivPost);
        final TextView textViewName = findViewById(R.id.tvName);
        final EditText textViewTitle = findViewById(R.id.tvTitle);
        final EditText editTextCaption = findViewById(R.id.etCaption);

        Intent intent = getIntent();

        type = intent.getStringExtra("type");
        desc = intent.getStringExtra("desc");
        isFutureMessage = intent.getBooleanExtra("isFuture",false);
        futureTimeInMillis = intent.getLongExtra("futureTimeInMillis",0);
        textViewTitle.setText(desc);
        if(intent.getStringExtra("type").equals("image")) {
            String uri = intent.getStringExtra("Bitmap");
            url = uri;
            Glide.with(this).load(uri).into(imageViewSelect);
//            Bitmap bitmap = null;
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                imageViewSelect.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }else {
            Uri uri = (Uri) intent.getParcelableExtra("Bitmap");
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
//            new VideoCompressAsyncTask(getApplicationContext(),picturePath).execute("false", capturedUri.toString(), f.getPath());

//            File f = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "/Silicompressor/videos");
//            if (f.mkdirs() || f.isDirectory()) {
//                //compress and output new video specs
//                new VideoCompressAsyncTask(this,uri).execute("true", picturePath, f.getPath());
//
//            }
            url = picturePath;
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
            imageViewSelect.setImageBitmap(bitmap);
        }
        if(!sharedPrefrencesMain.getProfileUrl().isEmpty())
            Glide.with(this).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);
        textViewName.setText(sharedPrefrencesMain.getName());

        imageViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String desc = "";
                String cap = "";
                if(!textViewTitle.getText().toString().isEmpty())
                    desc = textViewTitle.getText().toString();
                if(!editTextCaption.getText().toString().isEmpty())
                    cap = editTextCaption.getText().toString();
                if(type.equals("image")) {
                    callApiUploadImage(url,desc,"1",cap);
                }else if(type.equals("video")) {
                    callApiUploadImage(url,desc,"2",cap);
                }
            }
        });

    }

    private void callApiUploadImage(String path,String text,String type,String caption) {
        if (!path.isEmpty()){
            File file = new File(path);

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            MultipartBody.Part part;
            RequestBody requestBodyFdate=null;
            if(isFutureMessage && futureTimeInMillis!=null)
                requestBodyFdate = RequestBody.create(MediaType.parse("application/octet-stream"),futureTimeInMillis.toString());

            RequestBody requestBodyText = RequestBody.create(MediaType.parse("application/octet-stream"),text);
            RequestBody requestBodyType = RequestBody.create(MediaType.parse("application/octet-stream"),type);
            RequestBody requestBodyCode = RequestBody.create(MediaType.parse("application/octet-stream"),sharedPrefrencesMain.getFmCode());
            RequestBody requestBodyCaption = RequestBody.create(MediaType.parse("application/octet-stream"),caption);

            ProgressRequestBody fileBody;
            if(type.equals("1")) {
                fileBody = new ProgressRequestBody(file,"image", this);
                part = MultipartBody.Part.createFormData("image", file.getName(), fileBody);
            }
            else {
                fileBody = new ProgressRequestBody(file,"video", this);
                part = MultipartBody.Part.createFormData("video",file.getName(),fileBody);
            }
            Call<GetPost> apiResponseUploadImageCall = ApiClient.getClient().create(ApiInterface.class).createImagePost("Bearer "+sharedPrefrencesMain.getToken(),requestBodyCode,requestBodyText,requestBodyType,requestBodyCaption,requestBodyFdate,part);
            apiResponseUploadImageCall.enqueue(new Callback<GetPost>() {
                @Override
                public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                    pd.dismiss();
                    if (response.isSuccessful()){

                        if (response.body()!=null){
                            file.delete();
                            MediaScannerConnection.scanFile(PostActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                                /*
                                 *   (non-Javadoc)
                                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                                 */
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.e("ExternalStorage", "Scanned " + path + ":");
                                    Log.e("ExternalStorage", "-> uri=" + uri);
                                }
                            });
                            WallsFragment.ISUPDATED=true;
                            Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                    else{
                        Toast.makeText(AppContext.getAppContext(), ""+response.errorBody(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetPost> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(AppContext.getAppContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(AppContext.getAppContext(), "No such file Found", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }





    @Override
    public void onProgressUpdate(int percentage) {
        pd.setMessage(percentage+"% Uploading...");
        pd.show();

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

}