package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.SignUp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FamilySettingActivity extends AppCompatActivity implements View.OnClickListener {

    public static FamilySettingActivity familySettingActivity;
    SharedPrefrencesMain sharedPrefrencesMain;
    EditText editTextetDeceMember;
    boolean isVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_setting);

        familySettingActivity = this;
        sharedPrefrencesMain = new SharedPrefrencesMain(getApplicationContext());

        final ImageView imageView = findViewById(R.id.imgBack);
        final EditText etSwitchFamily = findViewById(R.id.etSwitch);
        final EditText etPenMembers = findViewById(R.id.etPenMembers);
        final EditText editTextCode = findViewById(R.id.etFamilyCodePswd);
        final EditText editTextNewFamily = findViewById(R.id.etNpassword);
        final EditText editTextUnjoinFamily = findViewById(R.id.etRNpassword);
        final ImageView imageViewCode = findViewById(R.id.ivCode);
        editTextetDeceMember = findViewById(R.id.etDeceMember);


        editTextCode.setText(sharedPrefrencesMain.getFmCode());

        imageView.setOnClickListener(this);
        etSwitchFamily.setOnClickListener(this);
        etPenMembers.setOnClickListener(this);
        editTextNewFamily.setOnClickListener(this);
        editTextUnjoinFamily.setOnClickListener(this);
        editTextetDeceMember.setOnClickListener(this);
        imageViewCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVisible) {
                    editTextCode.setTransformationMethod(new PasswordTransformationMethod());
                    isVisible = false;
                }else {
                    editTextCode.setTransformationMethod(null);
                    isVisible = true;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgBack) {
            finish();
        }else if(v.getId()==R.id.etSwitch){
            Intent intent = new Intent(getApplicationContext(),SwitchFamilyActivity.class);
            intent.putExtra("Type","Switch");
            startActivity(intent);
        }else if(v.getId()==R.id.etDeceMember){
            Intent intent = new Intent(getApplicationContext(),DecesedMembersActivity.class);
            startActivity(intent);
        }else if(v.getId()==R.id.etPenMembers){
//            Intent intent = new Intent(getApplicationContext(),PendingMembersActivity.class);
//            startActivity(intent);
        }else if(v.getId()==R.id.etNpassword){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(FamilySettingActivity.this);

            builder1.setTitle("Join new family");
            builder1.setMessage("Are you sure");
            builder1.setCancelable(false);
            final EditText input = new EditText(FamilySettingActivity.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setHint("Enter Family Code");
            input.requestFocus();
            input.setLayoutParams(lp);
            builder1.setView(input);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if(input.getText().toString().isEmpty()){
                                input.setError("Enter Family Code");
                                input.requestFocus();
                                Toast.makeText(getApplicationContext(),
                                        "Family Code Required!", Toast.LENGTH_SHORT).show();
                            }else {
                                hitAddFamilyApi(input.getText().toString());
                                dialog.cancel();
                            }
                        }
                    });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder1.show();
        }else if(v.getId()==R.id.etRNpassword){
            Intent intent = new Intent(getApplicationContext(),SwitchFamilyActivity.class);
            intent.putExtra("Type","Unjoin");
            startActivity(intent);
        }
    }
    public void hitAddFamilyApi(String fCode)
    {
        if(fCode.equals(sharedPrefrencesMain.getFmCode())) {
            Toast.makeText(AppContext.getAppContext(), "Can't remove your current family!", Toast.LENGTH_LONG).show();
            return;
        }
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.addfamilybycode("Bearer "+sharedPrefrencesMain.getToken(),fCode);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

}